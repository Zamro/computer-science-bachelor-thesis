#pragma once

template<unsigned srcWidth, unsigned srcHeight, unsigned destWidth, unsigned destHeight>
class DigitImageNormaliser
{
public:
    static std::array<ArithmeticType, destWidth*destHeight> normalise(const std::array<ArithmeticType, srcWidth*srcHeight>& input);
private:
    static std::array<ArithmeticType, 4*srcWidth*srcHeight> center(const std::array<ArithmeticType, srcWidth*srcHeight>& input);
    static std::array<ArithmeticType, destWidth*destHeight> scale(const std::array<ArithmeticType, 4*srcWidth*srcHeight>& input);
};

template<unsigned srcWidth, unsigned srcHeight, unsigned destWidth, unsigned destHeight>
std::array<ArithmeticType, destWidth*destHeight> DigitImageNormaliser<srcWidth, srcHeight, destWidth, destHeight>
    ::normalise(const std::array<ArithmeticType, srcWidth*srcHeight>& input)
{
    return scale(center(input));
}

template<unsigned srcWidth, unsigned srcHeight, unsigned destWidth, unsigned destHeight>
std::array<ArithmeticType, 4*srcWidth*srcHeight> DigitImageNormaliser<srcWidth, srcHeight, destWidth, destHeight>
    ::center(const std::array<ArithmeticType, srcWidth*srcHeight>& input)
{
    ArithmeticType sumX = 0, sumY = 0, sum = 0;
    for(unsigned y = 0; y < srcHeight; y++)
        for(unsigned x = 0; x < srcWidth; x++)
        {
            ArithmeticType point = input[y*srcWidth + x];
            sumX += x*point;
            sumY += y*point;
            sum += point;
        }
    unsigned centerX = sumX/sum;
    unsigned centerY = sumY/sum;

    std::array<ArithmeticType, 4*srcWidth*srcHeight> output;
    for(unsigned y = 0; y < 2 * srcHeight; y++)
    {
        int originalY = y + centerY - srcHeight;
        for(unsigned x = 0; x < 2 * srcWidth; x++)
        {
            int originalX = x + centerX - srcWidth;
            if(originalY >= 0 &&
               originalY < int(srcHeight) &&
               originalX >= 0 &&
               originalX < int(srcWidth))
                output[y * (2 * srcWidth) + x] = input[originalY * srcWidth + originalX];
            else
                output[y * (2 * srcWidth) + x] = 0;
        }
    }
    return output;
}

template<unsigned srcWidth, unsigned srcHeight, unsigned destWidth, unsigned destHeight>
std::array<ArithmeticType, destWidth*destHeight> DigitImageNormaliser<srcWidth, srcHeight, destWidth, destHeight>
    ::scale(const std::array<ArithmeticType, 4*srcWidth*srcHeight>& input)
{
    unsigned maxX = 0;
    unsigned minX = 2*srcWidth;
    unsigned maxY = 0;
    unsigned minY = 2*srcHeight;
    for(unsigned y = 0; y < 2 * srcHeight; y++)
        for(unsigned x = 0; x < 2 * srcWidth; x++)
            if(input[y * (2 * srcWidth) + x] != 0)
            {
                if(x > maxX) maxX = x;
                if(x < minX) minX = x;
                if(y > maxY) maxY = y;
                if(y < minY) minY = y;
            }
    double scaleX = double(destWidth)/(maxX - minX);
    double scaleY = double(destHeight)/(maxY - minY);

    double scale = scaleX < scaleY ? scaleX : scaleY;
    scale*=0.9;
//    std::cout<<scale<<std::endl;
    std::array<ArithmeticType, destWidth*destHeight> output;
    for(unsigned y = 0; y < destHeight; y++)
        for(unsigned x = 0; x < destWidth; x++)
        {
            double dummy;
            double srcY = (y - destHeight/2.)/scale + srcHeight;
            double srcX = (x - destWidth/2.)/scale + srcWidth;
            double xRatio = modf(srcX, &dummy);
            double yRatio = modf(srcY, &dummy);
            output[y * destWidth + x] = input[int(srcY) * srcWidth * 2 + int(srcX)]*(1-xRatio)*(1-yRatio);
            output[y * destWidth + x] += input[int(srcY+1) * srcWidth * 2 + int(srcX)]*xRatio*(1-yRatio);
            output[y * destWidth + x] += input[int(srcY) * srcWidth * 2 + int(srcX+1)]*(1-xRatio)*yRatio;
            output[y * destWidth + x] += input[int(srcY+1) * srcWidth * 2 + int(srcX+1)]*xRatio*yRatio;
        }
//    std::array<ArithmeticType, width*height> output;
//    for(unsigned y = 0; y < height; y++)
//        for(unsigned x = 0; x < width; x++)
//        {
//            output[y * width + x] = input[(y + height/2) * width * 2 + x + width/2];
//        }

    return output;
}


