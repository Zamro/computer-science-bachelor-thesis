TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp

HEADERS += \
    NeuralNetLib/BiasedNeuron.hpp \
    NeuralNetLib/LinearNeuron.hpp \
    NeuralNetLib/NeuralLayer.hpp \
    NeuralNetLib/NeuralNet.hpp \
    NeuralNetLib/Neuron.hpp \
    NeuralNetLib/ProbabilisticNeuralLayer.hpp \
    NeuralNetLib/SigmoidalNeuron.hpp \
    NeuralNetLib/SoftmaxNeuron.hpp \
    NeuralNetLib/ProbabilisticNeuralNet.hpp

QMAKE_CXXFLAGS += -std=c++11
