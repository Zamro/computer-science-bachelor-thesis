#include <cassert>
#include <iostream>
#include <cmath>
#include <numeric>

#include <NeuralNetLib/Neurons/Neuron.hpp>
#include <NeuralNetLib/Neurons/LinearNeuron.hpp>
#include <NeuralNetLib/Neurons/SigmoidalNeuron.hpp>
#include <NeuralNetLib/NeuralNet.hpp>
#include <NeuralNetLib/LearningAlgorithms/Quickprop.hpp>
#include <NeuralNetLib/LearningAlgorithms/WeightDecay.hpp>
#include <NeuralNetLib/Utilities.hpp>

double distance(double a, double b)
{
    return a<b ? b-a : a-b;
}

template<class T, size_t n>
double distance(const std::array<T, n>& a,const std::array<T, n>& b)
{
    double distance = 0;
    for(unsigned i=0;i<n;i++)
        distance += (b[i]-a[i])*(b[i]-a[i]);

    return sqrt(distance);
}

template<class T>
bool outputIsNear(T &n, double d, double tolerance = 0.1);

void assertNeuronImprovesOutput(Neuron<3> &neuron);

void testLearningLinearNeuron()
{
    std::cerr<<"testLearningLinearNeuron\n";
    LinearNeuron<3> neuron;
    neuron.setLearningAlgorithm(Backprop{{0.5, 0}});
    assertNeuronImprovesOutput(neuron);
    std::cerr<<"     ok\n";
}

void testLearningSigmoidalNeuron()
{
    std::cerr<<"testLearningSigmoidalNeuron\n";
    SigmoidalNeuron<3> neuron;
    neuron.setLearningAlgorithm(Backprop{{10, 0}});
    assertNeuronImprovesOutput(neuron);
    std::cerr<<"     ok\n";
}

void assertNeuronImprovesOutput(Neuron<3> &neuron)
{
    for (int i = 1; i < 10; i++)
    {
        neuron.setInput(std::array<double, 3>{1, 0, 0});
        neuron.learn(neuron.getOutput() - 0.3);
        neuron.finishBatch();

        neuron.setInput(std::array<double, 3>{0, 1, 0});
        neuron.learn(neuron.getOutput() - 0.5);
        neuron.finishBatch();

        neuron.setInput(std::array<double, 3>{0, 0, 1});
        neuron.learn(neuron.getOutput() - 0.1);
        neuron.finishBatch();
    }
    neuron.setInput(std::array<double, 3>{1, 0, 0});
//    std::cerr<<"0.3 : "<<neuron.getOutput()<<"\n";
    assert(outputIsNear(neuron, 0.3));

    neuron.setInput(std::array<double, 3>{0, 1, 0});
//    std::cerr<<"0.5 : "<<neuron.getOutput()<<"\n";
    assert(outputIsNear(neuron, 0.5));

    neuron.setInput(std::array<double, 3>{0, 0, 1});
//    std::cerr<<"0.1 : "<<neuron.getOutput()<<"\n";
    assert(outputIsNear(neuron, 0.1));
}

template<class T>
bool outputIsNear(T &n, double d, double tolerance)
{
    return distance(n.getOutput(), d) < tolerance;
}

void testCanCreateLayer()
{
    std::cerr<<"testCanCreateLayer\n";
    NeuralLayer<SigmoidalNeuron, 1, 1> layer;
    std::cerr<<"     ok\n";
}

void testLayerLearns()
{
    std::cerr<<"testLayerLearns\n";
    NeuralLayer<SigmoidalNeuron, 2, 3> layer;
    std::array<double, 2> input{1, 1};
    std::array<double, 3> properOutput{1, 1, 1};

    auto output = layer.predict(input);
    layer.train(input, properOutput);

    assert(distance(layer.predict(input), properOutput) < distance(output, properOutput));
    std::cerr<<"     ok\n";
}

void testCanCreateNeuralNet()
{
    std::cerr<<"testCanCreateNeuralNet\n";
    NeuralNet<SigmoidalNeuron, SigmoidalNeuron, 3, 3> net;
    std::cerr<<"     ok\n";
}

void testOneLayerNeuralNetLearns()
{
    std::cerr<<"testOneLayerNeuralNetLearns\n";
    NeuralNet<SigmoidalNeuron, SigmoidalNeuron, 2, 3> net;
    std::array<double, 2> input{1, 1};
    std::array<double, 3> properOutput{1, 1, 1};

    auto output = net.predict(input);
    net.train(input,properOutput);

    assert(distance(net.predict(input), properOutput) < distance(output, properOutput));
    std::cerr<<"     ok\n";
}

void testTwoLayersNeuralNetLearnsOneValue()
{
    std::cerr<<"testTwoLayersNeuralNetLearnsOneValue\n";
    NeuralNet<SigmoidalNeuron, SigmoidalNeuron,  2, 5, 4> net;
    std::array<double, 2> input{1, 1};
    std::array<double, 4> properOutput{1, 0, 1, 1};

    auto output = net.predict(input);
    net.train(input,properOutput);

    assert(distance(net.predict(input), properOutput) < distance(output, properOutput));
    std::cerr<<"     ok\n";
}

void testTwoLayersNeuralNetLearnsTwoValues()
{
    std::cerr<<"testTwoLayersNeuralNetLearnsTwoValues\n";
    NeuralNet<LinearNeuron, LinearNeuron,  2, 5, 4> net;
    net.setLearningAlgorithm(Backprop{{0.005, 0}});
    std::array<double, 2> firstInput{1,1};
    std::array<double, 4> firstProperOutput{1, 0, 1, 1};
    std::array<double, 2> secondInput{0,1};
    std::array<double, 4> secondProperOutput{1, 0, 0, 1};
    for(int i=0;i<10;i++)
    {
        auto firstOutput = net.predict(firstInput);
        auto secondOutput = net.predict(secondInput);
        net.train(firstInput, firstProperOutput);
        net.train(secondInput, secondProperOutput);

        assert(distance(net.predict(firstInput), firstProperOutput) + distance(net.predict(secondInput), secondProperOutput)
               < distance(firstOutput, firstProperOutput) + distance(secondOutput, secondProperOutput));
    }
    std::cerr<<"     ok\n";
}

void testTwoLayerProbabilisticNeuralNetLearnsAndPreservesSum()
{
    std::cerr<<"testTwoLayerProbabilisticNeuralNetLearns\n";
    NeuralNet<SigmoidalNeuron, SoftmaxNeuron,  2, 5, 4> net;
    net.setLearningAlgorithm(Backprop{{10, 0}});

    std::array<double, 2> input{1, 1};
    std::array<double, 4> properOutput{0, 1, 0, 0};

    auto oldOutput = net.predict(input);
    net.train(input, properOutput);
    auto newOutput = net.predict(input);

    assert(distance(newOutput, properOutput) < distance(oldOutput, properOutput) * 0.99);
    assert(distance(std::accumulate(newOutput.begin(), newOutput.end(), 0.), 1.) < 0.0001);
    std::cerr<<"     ok\n";
}

void testNeuralNetCanSaveStateToVector()
{
    std::cerr<<"testNeuralNetCanSaveStateToVector"<<std::endl;
    NeuralNet<SigmoidalNeuron, SigmoidalNeuron,  2, 5, 4> net;
    std::array<double, 2> input{1, 1};
    std::array<double, 4> properOutput{0, 0, 1, 0};

    net.train(input,properOutput);
    auto state = net.save();

    NeuralNet<SigmoidalNeuron, SigmoidalNeuron,  2, 5, 4> loadedNet;
    loadedNet.load(state.begin());

    assert(loadedNet.predict(input) == net.predict(input));
    std::cerr<<"     ok\n";
}

void testProbabilisticNeuralNetCanSaveStateToVector()
{
    std::cerr<<"testProbabilisticNeuralNetCanSaveStateToVector"<<std::endl;
    NeuralNet<SigmoidalNeuron, SoftmaxNeuron,  2, 5, 4> net;

    std::array<double, 2> input{1, 1};
    std::array<double, 4> properOutput{0, 0, 1, 0};

    net.train(input,properOutput);
    auto state = net.save();

    NeuralNet<SigmoidalNeuron, SoftmaxNeuron,  2, 5, 4> loadedNet;
    loadedNet.load(state.begin());

    assert(loadedNet.predict(input) == net.predict(input));
    std::cerr<<"     ok\n";
}

void testNeuralNetCanSaveStateToString()
{
    std::cerr<<"testNeuralNetCanSaveStateToString"<<std::endl;
    NeuralNet<SigmoidalNeuron, SigmoidalNeuron,  2, 5, 4> net;
    std::array<double, 2> input{1, 1};
    std::array<double, 4> properOutput{0, 0, 1, 0};
    net.train(input, properOutput);
    std::string state = net.saveToString();

    NeuralNet<SigmoidalNeuron, SigmoidalNeuron,  2, 5, 4> loadedNet;
    loadedNet.load(state);

    assert(loadedNet.predict(input) == net.predict(input));
    std::cerr<<"     ok\n";
}

void testNeuralLayerCanSaveStateToString()
{
    std::cerr<<"testNeuralLayerCanSaveStateToString"<<std::endl;
    NeuralNet<SigmoidalNeuron, SigmoidalNeuron, 2, 4> net;
    std::array<double, 2> input{1, 1};
    std::array<double, 4> properOutput{0, 0, 1, 0};
    net.train(input, properOutput);
    std::string state = net.saveToString();

    NeuralNet<SigmoidalNeuron, SigmoidalNeuron, 2, 4> loadedNet;
    loadedNet.load(state);

    assert(loadedNet.predict(input) == net.predict(input));
    std::cerr<<"     ok\n";
}

void testProbabilisticNeuralNetCanSaveStateToString()
{
    std::cerr<<"testProbabilisticNeuralNetCanSaveStateToString"<<std::endl;
    NeuralNet<SigmoidalNeuron, SoftmaxNeuron,  2, 5, 4> net;
    std::array<double, 2> input{1, 1};
    std::array<double, 4> properOutput{0, 0, 1, 0};
    net.train(input, properOutput);
    std::string state = net.saveToString();

    NeuralNet<SigmoidalNeuron, SoftmaxNeuron,  2, 5, 4> loadedNet;
    loadedNet.load(state);

    assert(loadedNet.predict(input) == net.predict(input));
    std::cerr<<"     ok\n";
}

int main()
{
    testLearningLinearNeuron();
    testLearningSigmoidalNeuron();
    testCanCreateLayer();
    testLayerLearns();
    testCanCreateNeuralNet();
    testOneLayerNeuralNetLearns();
    testTwoLayersNeuralNetLearnsOneValue();
    testTwoLayersNeuralNetLearnsTwoValues();
    testTwoLayerProbabilisticNeuralNetLearnsAndPreservesSum();
    testNeuralNetCanSaveStateToVector();
    testProbabilisticNeuralNetCanSaveStateToVector();
    testNeuralNetCanSaveStateToString();
    testProbabilisticNeuralNetCanSaveStateToString();
    testNeuralLayerCanSaveStateToString();

    return 0;
}
