#include "readerWriter.hpp"
#include "GlyphImage.hpp"
#include <sstream>
#include <QSet>
#include <QDir>
#include <QString>
#include <QFontDatabase>

std::array<double, imgWidth * imgHeight> loadDigit(int digit, int numberOfSample)
{
    std::array<double, imgWidth * imgHeight> array;
    std::array<unsigned char, imgWidth * imgHeight> byteArray;
    std::stringstream str;
    str<<"images/"<<digit<<"/"<<numberOfSample;
    File file(QString(str.str().data()), QIODevice::ReadOnly);
    file.read(reinterpret_cast<char*>(byteArray.begin()), imgWidth * imgHeight);
    for(unsigned int i=0;i <imgWidth * imgHeight; i++){
        array[i] = byteArray[i]/255.;
    }
    file.close();
    return array;
}

unsigned getNumberOfDigitSaves()
{
    unsigned minSave = -1; //max unsigned value
    for(unsigned digit = 0; digit < 10; digit++)
    {
        std::string imagesDirectoryPath = "images/" + std::to_string(digit);
        QDir imagesDirectory(imagesDirectoryPath.c_str());
        unsigned numberOfCurrentDigitSaves = imagesDirectory.exists() ? imagesDirectory.entryList(QDir::Files).count() : 0;
        minSave = std::min(minSave, numberOfCurrentDigitSaves);
    }
    return minSave;
}

void writeAllGeneratedDigits(const QSet<QString>& families)
{
    for(const auto& family:families)
    {
        std::cout<<"family: "<<family.toStdString()<<std::endl;
        for(const auto& system:QFontDatabase().writingSystems(family))
            std::cout<<"system: "<<QFontDatabase::writingSystemName(system).toStdString()<<std::endl;
        for(uchar letter = '0';letter<='9';letter++)
        {
            GlyphImage image(QFont(family), letter);
            image.writeToConsole();
        }
    }
}

void writeNetworkOutput(const std::array<double, 10>& output)
{
    std::cout.precision(2);
    std::cout<<std::fixed;
    for(auto out : output)
        std::cout<<out<<" ";
    std::cout<<std::endl;
}

void writeCsvHeading(QTextStream& out)
{
    // Not longer true - but would be perfect to be able to configure it
    // out<<"Long test;Learning rate*=sqrt(inputs);initial distribution = N(0,0.1)"<<endl;
    out<<"Long test"<<endl;
    out<<"Network;Momentum;learningRate;learningRateDrop;";
    for(unsigned j=0;j<NUMBER_OF_LEARNING_PASS;j++)
    {
        for(const QString& prefix : {"learn", "learn_high_confidence", "val", "val_high_confidence"})
        {
            for(unsigned i=0;i<10;i++)
                out<<"pass:" << j << ", "<<prefix<<" "<<i<<"%;";
            out<<"pass:" << j << ", "<<prefix<<" avg%;";
        }
    }
}

void writeStatisticsToCSV(QTextStream& out, const NetLearningResults& results)
{
    for(const auto& successRatioData : {results.successedLearning, results.successedValidation})
    {
        for(double ratio : successRatioData.getPerDigitSuccess())
        {
            out<<ratio<<";";
        }
        out<<successRatioData.getAvgSuccess()<<";";
        for(double ratio : successRatioData.getPerDigitConfidentSuccess())
        {
            out<<ratio<<";";
        }
        out<<successRatioData.getAvgConfidentSuccess()<<";";
    }
}
