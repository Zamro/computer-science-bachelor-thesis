#pragma once

unsigned const width = 32;
unsigned const height = 48;
unsigned const imgWidth = 32*2;
unsigned const imgHeight = 48*2;

const unsigned NUMBER_OF_LEARNING_PASS = 10;
const unsigned NUMBER_OF_FONTS_IN_LEARNING_BATCH = 250;
