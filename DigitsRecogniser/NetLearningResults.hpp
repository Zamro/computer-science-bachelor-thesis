#pragma once
#include <vector>
#include <algorithm>
#include "consts.hpp"

template<class T>
bool isOutputProper(const T& prediction, const T& properOutput)
{
    return std::max_element(prediction.begin(), prediction.end()) - prediction.begin()
        == std::max_element(properOutput.begin(), properOutput.end()) - properOutput.begin();
}

template<class T>
bool isOutputConfident(const T& prediction, const T& properOutput)
{
    return prediction[std::max_element(properOutput.begin(), properOutput.end()) - properOutput.begin()] > 0.95;
}

class SuccessRatioData{
    std::array<unsigned, 10> highConfidenceSuccessPerDigit{};
    std::array<unsigned, 10> successPerDigit{};
    unsigned count{};

    double getAvg(const std::array<unsigned, 10>& container) const
    {
        double successedAvg{};
        for(unsigned i = 0; i < 10; i++)
        {
            successedAvg += container[i]/(double(count));
        }
        successedAvg *= 0.1;
        return successedAvg;
    }

    std::array<double, 10> getSuccessRatioPerDigit(const std::array<unsigned, 10>& container) const
    {
        std::array<double, 10> out{};
        for(unsigned i = 0; i < 10; i++)
        {
            out[i] = container[i]/(double(count));
        }
        return out;
    }

public:

    double getAvgSuccess() const
    {
        return getAvg(successPerDigit);
    }

    double getAvgConfidentSuccess() const
    {
        return getAvg(highConfidenceSuccessPerDigit);
    }
    std::array<double, 10> getPerDigitSuccess() const
    {
        return getSuccessRatioPerDigit(successPerDigit);
    }

    std::array<double, 10> getPerDigitConfidentSuccess() const
    {
        return getSuccessRatioPerDigit(highConfidenceSuccessPerDigit);
    }

    template<class Net>
    void validateNetOutputs(Net& net, const typename Net::Data& data)
    {
        if(data.size() % 10 != 0)
            throw std::runtime_error("Batch of digits has length not divisible by 10!");
        for(unsigned i = 0; i < data.size(); ++i) {
            const auto& dataPair = data[i];
            const auto output = net.predict(dataPair.first);
            successPerDigit[i%10] += isOutputProper(output, dataPair.second);
            highConfidenceSuccessPerDigit[i%10] += isOutputConfident(output, dataPair.second);
        }
        count += data.size() / 10;
    }
};

struct NetLearningResults{
    SuccessRatioData successedLearning;
    SuccessRatioData successedValidation;
};
