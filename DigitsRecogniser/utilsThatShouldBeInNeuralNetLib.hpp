#pragma once

template<unsigned node>
std::string joinNodes(const std::string& separator = '_')
{
    return std::to_string(node);
}

template<unsigned node, unsigned next, unsigned... nodes>
std::string joinNodes(const std::string& separator = '_')
{
    return std::to_string(node) + separator + joinNodes<next, nodes...>(separator);
}

template<template<template<unsigned> class, template<unsigned> class, unsigned...> class Net,
    template<unsigned> class A, template<unsigned> class B, unsigned... nodes>
std::string getLayersDescription(const Net<A, B, nodes...>&, const std::string& separator = ", ")
{
    return joinNodes<nodes...>(separator);
}
