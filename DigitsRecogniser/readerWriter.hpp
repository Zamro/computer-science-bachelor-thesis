#pragma once
#include "consts.hpp"
#include "NetLearningResults.hpp"
#include "File.hpp"
#include <QTextStream>
#include <iostream>
#include <fstream>

unsigned getNumberOfDigitSaves();

std::array<double, imgWidth * imgHeight> loadDigit(int digit, int numberOfSample);

void writeAllGeneratedDigits(const QSet<QString>& families);

void writeNetworkOutput(const std::array<double, 10>& output);

void writeCsvHeading(QTextStream& out);

void writeStatisticsToCSV(QTextStream& out, const NetLearningResults& results);

template<unsigned width, unsigned height>
void writeDigit(const std::array<double, width*height>& array)
{
    for(unsigned y=0; y<height; y++)
    {
        for(unsigned x=0; x<width; x++)
            if(array[y*width+x]<0.25)
                std::cout<<uchar(219);
            else if(array[y*width+x]<0.5)
                std::cout<<'W';
            else if(array[y*width+x]<0.75)
                std::cout<<'I';
            else
                std::cout<<' ';
        std::cout<<std::endl;
    }
    std::cout<<std::endl;
}

template<class Net>
void writeNetSave(const Net &net, const QString& fileName)
{
    std::ofstream file(fileName.toUtf8(), std::ios_base::binary);
    const auto save = net.saveToString();
    unsigned length = save.length();
    file.write((reinterpret_cast<char*>(&length)), sizeof(length));
    file << save;

    // DEBUG writing for comparition in case of some problems on android reading side
    // std::cout<<"Writing "<<length<<"bytes: "<<std::endl;
    // for(const char c : save)
    //     std::cout<<int(c)<<" ";
    // std::cout<<std::endl;
}
