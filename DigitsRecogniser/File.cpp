#include "File.hpp"

File::File(const QString& filename, const QIODevice::OpenMode & mode) : QFile(filename){
    QFile::open(mode);
}

File::~File()
{
    QFile::close();
}
