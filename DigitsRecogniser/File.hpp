#pragma once
#include <QFile>
#include <QString>

struct File : QFile{
    File(const QString& filename, const QIODevice::OpenMode & mode);
    ~File() override;
};
