#include <GlyphImage.hpp>
#include <QPainter>
#include <iostream>

GlyphImage::GlyphImage(QFont font, uchar letter, double scale, uint width, uint height):
    width(width),
    height(height),
    tab(new uchar[getSizeInBytes()]),
    image(tab, width, height, QImage::Format_Mono)
{
    image.fill(0);
    QPainter painter(&image);
    font.setPixelSize(height*scale);
    painter.setFont(font);
    painter.drawText(image.rect(), Qt::AlignCenter, QString(letter));
}

GlyphImage::~GlyphImage()
{
    delete[] tab;
}

uint GlyphImage::getWidth() const
{
    uint addition = (width%32) ?
                32 - width%32 :
                0;
    return width + addition;
}

uint GlyphImage::getSizeInBytes() const
{
    return getWidth()/8*height;
}

bool GlyphImage::getBit(unsigned x, unsigned y) const
{
    uint pos = y*getWidth()+x;
    uchar byte = tab[pos/8];
    uchar mask = 1<<7;
    bool bit =(byte<<(pos%8))&mask;
    return bit;
}

uchar* GlyphImage::getBits() const
{
    return tab;
}

void GlyphImage::writeToConsole() const
{
    for(unsigned y=0; y<height; y++)
    {
        for(unsigned x=0; x<width; x++)
            std::cout<<uchar(getBit(x,y)?219:' ')<<" ";
        std::cout<<std::endl;
    }
}
