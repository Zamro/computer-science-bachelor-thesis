#include <QGuiApplication>
#include <QFontDatabase>
#include <QSet>

#include <iostream>
#include <algorithm>

#ifdef _WIN32
    //for saving files REALLY in binary format under windows.
    #include <fcntl.h>
    #include <io.h>
#endif

#include <NeuralNetLib/NeuralNet.hpp>
#include <NeuralNetLib/Neurons/SoftmaxNeuron.hpp>
#include <NeuralNetLib/Neurons/SigmoidalNeuron.hpp>
#include <NeuralNetLib/LearningAlgorithms/Backprop.hpp>
#include <NeuralNetLib/LearningAlgorithms/LearningAlgorithmFactory.hpp>

#include "utilsThatShouldBeInNeuralNetLib.hpp"
#include "File.hpp"
#include "GlyphImage.hpp"
#include "NetLearningResults.hpp"
#include "readerWriter.hpp"

//shared between AndroidApp and DigitsRecognizer apps, hence strange path
#include "../NeuralNet/NeuralNetLib/DigitImageNormaliser.hpp"

const QSet<QString> getFamilies()
{
    QStringList allFamilies = QFontDatabase().families(QFontDatabase::Latin);
    QStringList badFontFamilies = {"Small Fonts", "Fixedsys", "MS Serif", "System", "Jokerman"};
    return allFamilies.toSet().subtract(badFontFamilies.toSet());
}

std::array<double, 10> getProperOutputFor(unsigned id)
{
    std::array<double, 10> ret{{}};
    ret[id] = 1;
    return ret;
}

std::array<double, 10> getProperOutputFor(uchar letter)
{
    return getProperOutputFor(unsigned(letter - '0'));
}

std::array<double, width*height> convertToArray(const GlyphImage& image)
{
    std::array<double, imgWidth*imgHeight> array;
    for(unsigned j = 0; j < imgHeight;j++)
    {
        for(unsigned i = 0; i < imgWidth;i++)
        {
            array[i + j*imgWidth] = image.getBit(i,j);
        }
    }
    std::array<double, width*height> out = DigitImageNormaliser<imgWidth, imgHeight, width, height>::normalise(array);
    return out;
}


struct LearningParameters{
    double learningRate;
    double momentum;
    double learningRateDrop;
};

std::ostream& operator<<(std::ostream& out, LearningParameters parameters){
    out << "momentum = " << parameters.momentum << ", learningRate = " << parameters.learningRate
        << ", learningRateDrop = " << parameters.learningRateDrop;
    return out;
}

struct CrossValidationData {
    using Data = std::vector<std::pair<std::array<double, 1536>, std::array<double, 10> >>;
    std::vector<Data> learningSet;
    std::vector<Data> validationSet;
};

template <class Net>
NetLearningResults learnNetwork(Net& net,
                                const CrossValidationData& data,
                                LearningParameters& parameters //Ugly workaround for no option in NeuralNetLib to get or multiply current learningRate
                                )
{
    NetLearningResults results;

    for(const auto& data : data.learningSet){
        results.successedLearning.validateNetOutputs(net, data);

        net.train(data);
        parameters.learningRate *= parameters.learningRateDrop;
        net.setLearningAlgorithm(LearningAlgorithmFactory<Backprop>{{parameters.learningRate, parameters.momentum}});
    }

    for(const auto& data : data.validationSet){
        results.successedValidation.validateNetOutputs(net, data);
    }

    return results;
}

template <class Net>
Net createAndLearnNetworkWritingStatisticsToCSV(QTextStream& out,
                                                const CrossValidationData& data,
                                                LearningParameters parameters = {0.5, 0.8, 0.98})
{
    Net net;
    out << endl << getLayersDescription(net).c_str() << ";";
    std::cout << "learning new NeuralNet<" << getLayersDescription(net) << "> for " << NUMBER_OF_LEARNING_PASS << " passes \n"
        << "with " << parameters << std::endl;

    net.setLearningAlgorithm(LearningAlgorithmFactory<Backprop>{{parameters.learningRate, parameters.momentum}});

    out << parameters.momentum << ";" << parameters.learningRate << ";" << parameters.learningRateDrop << ";";
    for(unsigned passNumber = 0; passNumber < NUMBER_OF_LEARNING_PASS; passNumber++)
    {
        NetLearningResults results = learnNetwork(net, data, parameters);
        writeStatisticsToCSV(out, results);
        std::cout << "  pass: " << passNumber << " learningSuccessRate: (best choose)" << results.successedLearning.getAvgSuccess() << " (95% confidence) " << results.successedLearning.getAvgConfidentSuccess()
            << ", validationSuccessRate: (best choose)" << results.successedValidation.getAvgSuccess() << " (95% confidence) " << results.successedValidation.getAvgConfidentSuccess()
             << std::endl;
    }
    return net;
}


void testNetworks(QTextStream& out, const CrossValidationData& data, LearningParameters parameters)
{
    createAndLearnNetworkWritingStatisticsToCSV<NeuralNet<SigmoidalNeuron, SoftmaxNeuron, width * height, 10>>(out, data, parameters);
//    createAndLearnNetworkWritingStatisticsToCSV<NeuralNet<SigmoidalNeuron, SoftmaxNeuron, width * height, 8, 10>>(out, data, parameters);
//    createAndLearnNetworkWritingStatisticsToCSV<NeuralNet<SigmoidalNeuron, SoftmaxNeuron, width * height, 16, 10>>(out, data, parameters);
    createAndLearnNetworkWritingStatisticsToCSV<NeuralNet<SigmoidalNeuron, SoftmaxNeuron, width * height, 32, 10>>(out, data, parameters);
//    createAndLearnNetworkWritingStatisticsToCSV<NeuralNet<SigmoidalNeuron, SoftmaxNeuron, width * height, 64, 10>>(out, data, parameters);
//    createAndLearnNetworkWritingStatisticsToCSV<NeuralNet<SigmoidalNeuron, SoftmaxNeuron, width * height, 128, 10>>(out, data, parameters);
}

void testNetworks(const CrossValidationData& data, const QString& filename)
{
    File file(filename, QIODevice::WriteOnly);
    QTextStream out(&file);
    writeCsvHeading(out);
    std::vector<LearningParameters> parametersList{};
    for(double momentum = 0.3; momentum <= 0.9; momentum += 0.3)//3
    {
        for(double learningRate = 8; learningRate >= 0.5; learningRate/=2) //5
        {
            std::vector<double> learningRateDrops = {0.997, 0.998, 1.0};  //3

            for(const auto learningRateDrop : learningRateDrops){
                parametersList.push_back({learningRate, momentum, learningRateDrop});
            }
        }
    }

    for(unsigned i = 0; i < parametersList.size(); ++i){
        std::cout<<"Testing parameters set " << i + 1 << "/" << parametersList.size() << std::endl;
        testNetworks(out, data, parametersList[i]);
    }
}

template <class Net>
Net createandLearngNetworkUntilGivenSuccesValidationRate(const CrossValidationData& data,
                                                         double successValidationRate,
                                                         LearningParameters parameters)
{
    std::cout << "learning until given validation rate \n" << "with " << parameters << std::endl;
    while(true)
    {
        Net net{};
        net.setLearningAlgorithm(LearningAlgorithmFactory<Backprop>{{parameters.learningRate, parameters.momentum}});
        NetLearningResults results;
        int passNumber{};
        std::cout << "Created new NeuralNet<" << getLayersDescription(net) <<"> " << std::endl;
        do
        {
            results = learnNetwork(net, data, parameters);
            std::cout << "  pass: " << passNumber << " learningSuccessRate: (best choose)" << results.successedLearning.getAvgSuccess() << " (95% confidence) " << results.successedLearning.getAvgConfidentSuccess()
                << ", validationSuccessRate: (best choose)" << results.successedValidation.getAvgSuccess() << " (95% confidence) " << results.successedValidation.getAvgConfidentSuccess()
                << std::endl;
            ++passNumber;
        } while(results.successedValidation.getAvgConfidentSuccess() < successValidationRate
            && (results.successedValidation.getAvgSuccess() > 0.75 || passNumber < 4)
            && (results.successedValidation.getAvgSuccess() > 0.8 || passNumber < 10)
            && passNumber < 30);

        if(results.successedValidation.getAvgConfidentSuccess() >= successValidationRate){
            std::cout << " Success!" << std::endl;
            return net;
        }
        std::cout << " Failure, starting from scratch!" << std::endl;
        //TODO: bring back shuffling after changing data format - note necessarli needed, as for now it seems, that it is never coming into failure.
        // unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        // shuffle (familiesList.begin(), familiesList.end(), std::default_random_engine(seed));
    }
}

void generateSingleLayerNetwork(const CrossValidationData& data,  const QString& filename)
{
    writeNetSave(
        createandLearngNetworkUntilGivenSuccesValidationRate<NeuralNet<SigmoidalNeuron, SoftmaxNeuron, width * height, 10>>(
            data,
            0.98,
            {1, 0.6, 1}
        ), filename);
}

void generateDoubleLayerNetwork(const CrossValidationData& data,  const QString& filename)
{
    writeNetSave(
        createandLearngNetworkUntilGivenSuccesValidationRate<NeuralNet<SigmoidalNeuron, SoftmaxNeuron, width * height, 32, 10>>(
            data,
            0.92,
            {1, 0.6, 1}
        ), filename);
}

CrossValidationData prepareCrossValidationGlyphs(const QSet<QString>& families)
{
    CrossValidationData data{};
    for(const auto& family: families)
    {
        auto& set = (data.learningSet.size() < NUMBER_OF_FONTS_IN_LEARNING_BATCH) ? data.learningSet : data.validationSet;
        auto& batch = set.emplace_back();
        for(uchar letter = '0';letter<='9';letter++)
        {
            GlyphImage image(QFont(family), letter, 0.9, imgWidth, imgHeight);
            auto array = convertToArray(image);
            auto properOut = getProperOutputFor(letter);
            batch.emplace_back(std::move(array), std::move(properOut));
        }
    }
    return data;
}

void enrichWithHandWrittenSaves(CrossValidationData& data)
{
    unsigned numberOfDigitSaves = getNumberOfDigitSaves();
    if(numberOfDigitSaves == 0){
        std::cout << "No full set of hand written digits included. Remember, you can copy them from AndroidApplication into the images/[digit] path."<<std::endl;
        return;
    }

    std::cout << "reading " << numberOfDigitSaves << "hand written digits" << std::endl;
    for(unsigned numberOfSample = 0; numberOfSample < numberOfDigitSaves; numberOfSample++)
    {
        auto& batch = data.learningSet.emplace_back();
        for(unsigned digit = 0; digit<10;digit++)
        {
            auto array = loadDigit(digit, numberOfSample);
            auto normalised = DigitImageNormaliser<imgWidth, imgHeight, width, height>::normalise(array);
            auto properOut = getProperOutputFor(digit);
            batch.emplace_back(std::move(normalised), std::move(properOut));
        }
    }
}

int main(int argc, char *argv[])
{
    #ifdef _WIN32
        //for saving files REALLY in binary format under windows. 1 for STDOUT, so I do not remember, why it was needed.
        _setmode(1,_O_BINARY);
    #endif

    QGuiApplication app(argc, argv);
    auto families = getFamilies();
    auto glyphs = prepareCrossValidationGlyphs(families);
    enrichWithHandWrittenSaves(glyphs);
    std::cout << "Generating \"best\" networks with given parameters:" << std::endl;
    generateSingleLayerNetwork(glyphs, "singleLayerNetwork_948");
    generateDoubleLayerNetwork(glyphs, "doubleLayerNetwork_94");
    std::cout << "Testing a wide range of parameters:" << std::endl;
    testNetworks(glyphs, "testResults.csv");
    return 0;
}
