#pragma once
#include <QImage>
#include <QFont>

class GlyphImage
{
public:
    GlyphImage(QFont font, uchar letter, double scale = 0.9, uint width = 32, uint height = 48);
    ~GlyphImage();
    bool getBit(unsigned x, unsigned y) const;
    void writeToConsole() const;
private:
    unsigned width;
    unsigned height;
    uchar *tab;
    QImage image;
    uint getWidth() const;
    uint getSizeInBytes() const;
    uchar* getBits() const;
};
