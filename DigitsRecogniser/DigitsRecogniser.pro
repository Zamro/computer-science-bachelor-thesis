#-------------------------------------------------
#
# Project created by QtCreator 2014-12-13T15:45:23
#
#-------------------------------------------------

QT       += core

TARGET = DigitsRecogniser
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    GlyphImage.cpp \
    readerWriter.cpp

HEADERS += \
    GlyphImage.hpp\
    NeuralNetLib/SoftmaxNeuron.hpp\
    NeuralNetLib/Neuron.hpp\
    NeuralNetLib/NeuralNet.hpp \
    NeuralNetLib/DigitImageNormaliser.hpp \
    NetLearningResults.hpp \
    readerWriter.hpp \
    consts.hpp

QMAKE_CXXFLAGS += -std=c++11
