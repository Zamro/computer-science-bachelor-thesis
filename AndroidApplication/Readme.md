To build this application, proper NeuralNetLib library must be provided.

At the moment of writing this document, it can be configured and compiled with below command:

    cmake -DCMAKE_TOOLCHAIN_FILE=~/Android/Sdk/ndk/22.0.6917172/build/cmake/android.toolchain.cmake -DANDROID_ABI=armeabi-v7a -DANDROID_NATIVE_API_LEVEL=16 -DCMAKE_INSTALL_PREFIX=[install_dir] [neuralNetLib.git]
    cmake --build . --target NeuralNetLib --target NeuralNetLib_UT_TEST_UTILS
    cmake --install .

Assuming, that proper ndk is installed by Android Studio in ~/Android/Sdk/ndk/22.0.6917172. It can be also downloaded manually. Ndk version must be at least 22 to correctly support C++11 filesystem library.
 - `[install_dir]` can be any chosen directory, which must be later set as cmake flag in ./app/build.gradle

        externalNativeBuild {
            cmake {
                cppFlags "-std=c++17"
                arguments "-DNEURALNETLIB_PATH=[install_dir]"
            }
        }

 - `[neuralNetLib.git]` is directory of NeuralNetLib sources catalog, which must be checked out from https://bitbucket.org/Zamro/neuralnetlib (git@bitbucket.org:Zamro/neuralnetlib.git)

For full compatibility procedure must be repeated for all android ABIs:

    cmake -DANDROID_ABI=arm64-v8a ...
    cmake -DANDROID_ABI=armeabi-v7a ...
    cmake -DANDROID_ABI=x86 ...
    cmake -DANDROID_ABI=x86_64 ..

For using on a single device, just a single ABI needs to be compiled. As for now using multiple ABIs has not be tested by me, it could possibly need 4 different install_dirs and hence some modifications inside ./app/src/main/jni/CMakeLists.txt to make choosing the installation folder dynamic.