package com.example.pawel.neuralnetexample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Pawel on 2015-02-08.
 */
public class BatchInfoAdapter extends ArrayAdapter<BatchInfo> {
    private List<BatchInfo> batches;
    private Context context;

    public BatchInfoAdapter(Context context, int resource, List<BatchInfo> batches) {
        super(context, resource, batches);
        this.context = context;
        this.batches = batches;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.batch_list_item, null);
        }

        final BatchInfo batch = batches.get(position);
        if (batch != null) {
            ((TextView) view.findViewById(R.id.nameTextView)).setText(batch.name + " (" + batch.numberOfRecordings + ")");
            ((CheckBox) view.findViewById(R.id.learningCheckBox)).setChecked(batch.isInLearningSet);
            ((CheckBox) view.findViewById(R.id.validationCheckBox)).setChecked(batch.isInValidationSet);
            view.findViewById(R.id.learningCheckBox).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    batch.isInLearningSet = !batch.isInLearningSet;
                }
            });
            view.findViewById(R.id.validationCheckBox).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    batch.isInValidationSet = !batch.isInValidationSet;
                }
            });
            view.findViewById(R.id.playButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    batch.getBatch().play();
                }
            });
            view.findViewById(R.id.removeButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    batches.remove(batch);
                    batch.delete();
                    notifyDataSetChanged();
                }
            });
        }
        return view;
    }

    @Override
    public void remove(BatchInfo info){

        batches.remove(info);
        super.remove(info);
    }

    public BatchInfo getItem(int position){
        return batches.get(position);
    }
}
