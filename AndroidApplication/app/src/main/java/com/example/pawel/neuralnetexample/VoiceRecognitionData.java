package com.example.pawel.neuralnetexample;

import java.io.Serializable;

/**
 * Created by Pawel on 2015-02-07.
 */
public class VoiceRecognitionData implements Serializable{
    private short[] audioData;
    private int properFeedback;

    VoiceRecognitionData(short[] audioData, int properFeedback)
    {
        this.audioData = audioData;
        this.properFeedback = properFeedback;
    }


    public short[] getAudioData() {
        return audioData;
    }

    public int getProperFeedback() {
        return properFeedback;
    }
}
