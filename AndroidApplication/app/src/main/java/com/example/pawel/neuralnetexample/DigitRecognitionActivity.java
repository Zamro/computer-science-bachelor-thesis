package com.example.pawel.neuralnetexample;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

public class DigitRecognitionActivity extends Activity {
    PaintDigitView paintView;
    ArrayList<LinearLayout> buttonLayouts = new ArrayList<>();
    ArrayList<Button> buttons = new ArrayList<>();;

    private final String fileName = "digitsNeuralNet.save";
    DigitRecognitionNeuralNet net = new DigitRecognitionNeuralNet();
    static {
        System.loadLibrary("NeuralNet");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_digit_recognition);
        paintView = findViewById(R.id.paintDigitView);
        buttonLayouts.add(findViewById(R.id.digitButtonsLayout1));
        buttonLayouts.add(findViewById(R.id.digitButtonsLayout2));

        for(LinearLayout layout : buttonLayouts) {
            for (int i = 0; i < layout.getChildCount(); i++) {
                buttons.add((Button) layout.getChildAt(i));
            }
        }

        loadNeuralNet();
    }

    private void loadNeuralNet() {
        File file = new File(getExternalFilesDir(null), fileName);
        if( !file.exists())
        {
            Log.i("Loading net:","creating new net");
            loadNetFromRawResource(net, R.raw.single_layer_handwritten98);
        }
        else
        {
            Log.i("Loading net:","loading existing net");
            loadNetFromFile(net,  new File(getExternalFilesDir(null), fileName));
        }
        paintView.setNeuralNet(net);
    }

    public void giveFeedback(View clickedButton) {
        int feedbackButonId = Integer.parseInt((String) clickedButton.getTag());
        net.train(paintView.getPixels(), feedbackButonId);
        saveData(feedbackButonId);
        clearData(null);
    }

    private void saveData(int number){
        File imgsDir;
        imgsDir = new File(getExternalFilesDir(null), "images");
        if (!imgsDir.exists())
            imgsDir.mkdir();
        File numberDir = new File(imgsDir, "" + number);
        if (!numberDir.exists())
            numberDir.mkdir();
        int i = 0;
        File img;
        do{
            img = new File(numberDir, "" + i++);
        }while(img.exists());
        try {
            FileOutputStream fileStream = new FileOutputStream(img);
            double pixels[] = paintView.getPixels();
            for(double pixel:pixels)
            {
                fileStream.write((byte)(255 * pixel));
            }
            fileStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void clearData(View view) {
        paintView.clear();
        setOutput(new double[10]);
        for(Button button : buttons) {
            button.setClickable(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy()
    {
        saveNetToFile(net, new File(getExternalFilesDir(null), fileName));
        net.destroy();
        super.onDestroy();
    }

    private void loadNetFromStream(DigitRecognitionNeuralNet net, InputStream input){
        try {
            byte lengthTable[] = new byte[4];
            input.read(lengthTable);
            ByteBuffer buffer = ByteBuffer.wrap(lengthTable);
            buffer.order(ByteOrder.LITTLE_ENDIAN);
            int length = buffer.getInt();
            byte[] data = new byte[length];
            if(input.read(data) != length)
                Log.e("LOAD", "loadNetFromStream: read length is different than expected!");

// DEBUG reading for comparison in case of some problems
//            Log.i("LOAD", "loadNetFromStream: length = " + length);
//            StringBuilder str = new StringBuilder();
//            for(byte b : data)
//                str.append(Integer.valueOf(b)).append(" ");
//            Log.i("LOAD", "loadNetFromStream: bytes: " + str.toString());

            net.load(data);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void loadNetFromRawResource(DigitRecognitionNeuralNet net, int fileID) {
        try {
            InputStream input = getResources().openRawResource(fileID);
            loadNetFromStream(net, input);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void saveNetToFile(DigitRecognitionNeuralNet net, File file) {
        byte[] save = net.save();
        try {
            FileOutputStream out = new FileOutputStream(file);
            byte lengthTable[] = new byte[4];
            ByteBuffer buffer = ByteBuffer.wrap(lengthTable);
            buffer.order(ByteOrder.LITTLE_ENDIAN);
            buffer.putInt(save.length);
            out.write(lengthTable);
            out.write(save);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadNetFromFile(DigitRecognitionNeuralNet net, File file) {
        try {
            FileInputStream input = new FileInputStream(file);
            loadNetFromStream(net, input);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setOutput(double[] output)
    {
        for(int i = 0; i < buttons.size(); i++) {
            Button button = buttons.get(i);
            button.getBackground().setColorFilter(Color.argb((int) (output[i] * 255), 0, 255, 0), PorterDuff.Mode.SRC);
            button.setText(String.format("%d\n%.1f%%\n", i, 100 * output[i]));
            button.setClickable(true);
        }
        for(LinearLayout layout : buttonLayouts)
            layout.invalidate();
    }
}
