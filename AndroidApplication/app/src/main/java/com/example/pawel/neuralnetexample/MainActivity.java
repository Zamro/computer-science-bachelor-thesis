package com.example.pawel.neuralnetexample;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;


public class MainActivity extends Activity {

    private boolean checkVoicePermision() {
        boolean permitted = ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED;
        if (!permitted) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECORD_AUDIO},
                    1234);
        }
        return permitted;
    }

    public void startVoiceRecognition(View view) {
        if (checkVoicePermision())
        {
            Intent intent = new Intent(this, VoiceRecognitionActivity.class);
            startActivity(intent);
        }
    }

    public void startDigitRecognition(View view) {
        Intent intent = new Intent(this, DigitRecognitionActivity.class);
        startActivity(intent);
    }

    public void startBatchVoiceRecognition(View view) {
        if (checkVoicePermision())
        {
            Intent intent = new Intent(this, BatchVoiceRecognitionActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
