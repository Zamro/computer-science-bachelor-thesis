package com.example.pawel.neuralnetexample;

/**
 * Created by Pawel on 2015-01-09.
 */
public class DigitRecognitionNeuralNet {
    private long address;

    public DigitRecognitionNeuralNet()
    {
        address = getDigitRecognitionNeuralNet();
    }

    private native long getDigitRecognitionNeuralNet();

    public void destroy()
    {
        destroyNative(address);
    }

    private native void destroyNative(long address);

    public byte[] save()
    {
        return saveNative(address);
    }

    private native byte[] saveNative(long address);

    public void load(byte[] input)
    {
        loadNative(address, input);
    }

    private native void loadNative(long address, byte[] input);

    public double[] recognise(double input[])
    {
        return recogniseNative(address, input);
    }

    private native double[] recogniseNative(long address, double input[]);

    public double train(double input[], int feedback)
    {
        return trainNative(address, input, feedback);
    }

    private native double trainNative(long address, double input[], int feedback);
}
