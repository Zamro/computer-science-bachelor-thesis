package com.example.pawel.neuralnetexample;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import androidx.core.app.NavUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

public class NewBatchActivity extends Activity {
    VoiceRecognitionData[] currentRecordings = new VoiceRecognitionData[5];
    ArrayList<VoiceRecognitionData> batch = new ArrayList<>();
    VoiceRecordingManager voiceRecordingManager = new VoiceRecordingManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_batch);
    }

    public void record(final View view){
        new Thread(){
            @Override
            public void run(){
                int feedback = Integer.parseInt((String) view.getTag());
                currentRecordings[feedback] =
                        new VoiceRecognitionData(voiceRecordingManager.record(), feedback);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        view.getBackground().setColorFilter(Color.argb(255, 0, 255, 0), PorterDuff.Mode.DARKEN);

                        view.invalidate();
                        View button = findViewById(R.id.saveBatchPartButton);
                        button.setEnabled(areRecordingsValid());
                        button.invalidate();
                    }
                });
            }
        }.start();
    }

    private boolean areRecordingsValid(){
        boolean isValid = true;
        for(VoiceRecognitionData data : currentRecordings)
            isValid = isValid && (data != null);
        return isValid;
    }

    public void saveBatchPart(View view){
        view.setEnabled(false);
        view.invalidate();
        Collections.addAll(batch, currentRecordings);
        currentRecordings = new VoiceRecognitionData[5];
        LinearLayout layout = (LinearLayout) findViewById(R.id.voiceButtonsLayout);
        for (int i = 0; i < layout.getChildCount(); i++) {
            layout.getChildAt(i).getBackground().setColorFilter(Color.argb(0, 0, 0, 0), PorterDuff.Mode.DARKEN);
        }
        layout.invalidate();
        View button = findViewById(R.id.saveBatchButton);
        button.setEnabled(true);
        button.invalidate();
    }

    public void saveBatch(View view){
        view.setEnabled(false);
        view.invalidate();
        String batchesFileName = ((TextView)findViewById(R.id.batchNameTextField)).getText().toString();
        if(!batchesFileName.isEmpty()){
            final File dir = new File(getExternalFilesDir(null), "batches");
            final File file = new File(dir, batchesFileName);

            new Thread() {
                @Override
                public void run() {
                    new Batch(batch).save(file);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            NavUtils.navigateUpFromSameTask(NewBatchActivity.this);

                        }
                    });
                }
            }.start();
        }
    }

    @Override
    public void onDestroy(){
        voiceRecordingManager.release();
        super.onDestroy();
    }
}
