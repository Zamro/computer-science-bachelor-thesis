package com.example.pawel.neuralnetexample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.function.Function;

import static java.lang.Math.sqrt;

public class BatchVoiceRecognitionActivity extends Activity {
    private final String netFileName = "batchVoiceNeuralNet.save";
    private final String shiftFileName = "batchShift.save";
    private final LinkedList<String> logList = new LinkedList<>();
    private boolean isNotAborted;
    ArrayAdapter<String> adapter;
    ListView listView;

    VoiceRecognitionNeuralNet net;
    short[] shift;
    private final int numberOfPassesBetweenLearningRateUpdate = 5;
    private double[] errorDiffs = new double[numberOfPassesBetweenLearningRateUpdate];


    int TABLE_SIZE = VoiceRecordingManager.TABLE_SIZE;

    static {
        System.loadLibrary("NeuralNet");
    }

    public void batches(View view){
        Intent intent = new Intent(this, BatchesActivity.class);
        startActivity(intent);
    }

    public void reset(View view){
        new Thread() {
            @Override
            public void run() {
                setStatusAndLog("Creating new network...");
                setGuiBlocked(true);
                if (net != null) net.destroy();
                net = new VoiceRecognitionNeuralNet();
                calculateShift();
                setNetLoaded();
                setStatusAndLog("Network created");
                setGuiBlocked(false);
            }
        }.start();
    }

    public void load(View view){
        new Thread(){
            @Override
            public void run(){
                File file = new File(getExternalFilesDir(null), netFileName);
                if(file.exists())
                {
                    setStatusAndLog("Loading network");
                    setGuiBlocked(true);
                    if(net!= null) net.destroy();
                    try {
                        FileInputStream input = new FileInputStream(file);
                        loadNetFromStream(input);
                        loadShift();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    setNetLoaded();
                    setStatusAndLog("Network loaded");
                    setGuiBlocked(false);
                }else{
                    setStatus("Network not found, please create new one");
                }
            }
        }.start();
    }

    private void setNetLoaded(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.learnNetworkButton).setEnabled(net != null);
                findViewById(R.id.saveNetworkButton).setEnabled(net != null);
            }
        });
    }

    public void save(View view){
        new Thread() {
            @Override
            public void run() {
                setStatusAndLog("Saving network");
                setGuiBlocked(true);
                saveNetToFile();
                saveShift();
                setGuiBlocked(false);
                setStatusAndLog("Network saved");
                clearStatus();
            }
        }.start();
    }

    private void setLearningInGui(final boolean isLearning)
    {
        setGuiBlocked(isLearning);
        runOnUiThread(new Runnable(){
            @Override
            public void run() {
                int learnVisibility = isLearning ? View.GONE : View.VISIBLE;
                int abortVisibility = isLearning ? View.VISIBLE : View.GONE;
                findViewById(R.id.learnNetworkButton).setVisibility(learnVisibility);
                findViewById(R.id.abortLearningNetworkButton).setVisibility(abortVisibility);
            }
        });
    }

    private void setGuiBlocked(final boolean isBlocked)
    {
        runOnUiThread(new Runnable(){
            @Override
            public void run() {
                findViewById(R.id.batchesButton).setEnabled(!isBlocked);
                findViewById(R.id.createNetworkButton).setEnabled(!isBlocked);
                findViewById(R.id.loadNetworkButton).setEnabled(!isBlocked);
                findViewById(R.id.saveNetworkButton).setEnabled((!isBlocked) && net!= null);
                findViewById(R.id.learningRateEditText).setEnabled(!isBlocked);
                findViewById(R.id.numberOfPassesEditText).setEnabled(!isBlocked);
                findViewById(R.id.learnNetworkButton).setEnabled((!isBlocked) && net!= null);
            }
        });
    }

    private double processSet(ArrayList<BatchNative> set, int pass, int passes, int batchNumber, int allBatches, Function<Pair<VoiceRecognitionNeuralNet, BatchNative>, Double> function)
    {
        double error = 0;
        int recordsCount = 0;
        for (BatchNative batch : set) {
            if (isNotAborted) {
                recordsCount += batch.batchSize;
                error += function.apply(new Pair<>(net, batch));
                updateStatus(pass, passes, batchNumber, allBatches, batch.batchSize);
            }
            batchNumber++;
        }
        return recordsCount > 0 ? error / recordsCount : 0;
    }

    private double learnSet(ArrayList<BatchNative> set, int pass, int passes, int batchNumber, int allBatches)
    {
        return processSet(set, pass, passes, batchNumber, allBatches, (Pair<VoiceRecognitionNeuralNet, BatchNative> p) -> p.first.trainBatch(p.second));
    }

    private double validateSet(ArrayList<BatchNative> set, int pass, int passes, int batchNumber, int allBatches)
    {
        return processSet(set, pass, passes, batchNumber, allBatches, (Pair<VoiceRecognitionNeuralNet, BatchNative> p) -> p.first.batchValidationError(p.second));
    }

    public void learnAll(View button)
    {
        isNotAborted = true;
        final double learningRate = getLearningRateFromGui();
        final double momentum = getMomentumFromGui();
        final int passes= Integer.parseInt(((TextView)findViewById(R.id.numberOfPassesEditText)).getText().toString());
        new Thread(){
            @Override
            public void run(){
                setLearningInGui(true);
                log("Learning started");
                setStatusAndLog("   Loading audio data...");
                ArrayList<BatchInfo> batches = BatchesLoader.getBatchList(BatchVoiceRecognitionActivity.this);
                ArrayList<BatchNative> learningSet = new ArrayList<>();
                ArrayList<BatchNative> validationSet = new ArrayList<>();

                for (BatchInfo batchInfo : batches) {
                    if(batchInfo.isInLearningSet)
                        learningSet.add(new BatchNative(batchInfo.getBatch()));
                    if(batchInfo.isInValidationSet)
                        validationSet.add(new BatchNative(batchInfo.getBatch()));
                }
                setLearningRate(learningRate, momentum);
                double prevValidationError = 0;
                double prevLearningError = 0;
                for(int j=0;j<passes;j++) {
                    if(isNotAborted) {
                        log(" pass: " + (j + 1) + " / " + passes);

                        double learningError = learnSet(learningSet, j, passes, 0, learningSet.size() + validationSet.size());
                        if (learningError != 0) {
                            log("     learning error: " + getComparison(learningError, prevLearningError));
                            prevLearningError = learningError;
                        }

                        double validationError = validateSet(validationSet, j, passes, learningSet.size(), learningSet.size() + validationSet.size());
                        if (validationError != 0) {
                            log("     validation error: " + getComparison(validationError, prevValidationError));
                            errorDiffs[j%numberOfPassesBetweenLearningRateUpdate] = (prevValidationError - validationError)/validationError;
                            if((j+1)%numberOfPassesBetweenLearningRateUpdate == 0){
                                updateLearningRate();
                            }
                            prevValidationError = validationError;
                        }
                    }
                }
                setLearningInGui(false);
                setStatusAndLog("Learning finished");
                clearStatus();
            }
        }.start();
    }

    private double getLearningRateFromGui() {
        return Double.parseDouble(((TextView)findViewById(R.id.learningRateEditText)).getText().toString());
    }

    private double getMomentumFromGui() {
        return Double.parseDouble(((TextView)findViewById(R.id.momentumEditText)).getText().toString());
    }

    private boolean shouldDecreaseLearningRate(){
        Arrays.sort(errorDiffs);
        return errorDiffs[1] < 0;
    }

    private void updateLearningRate()
    {
        double learningRate = getLearningRateFromGui();
        if(shouldDecreaseLearningRate())
            learningRate *= 0.9;
        else
            learningRate *= 1.2;

        final double momentum = getMomentumFromGui();
        setLearningRate(learningRate, momentum);
    }

    private void setLearningRate(final double learningRate, double momentum){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((TextView) findViewById(R.id.learningRateEditText)).setText("" + learningRate);
            }
        });
        log("   Learning rate: " + learningRate + ", momentum: " + momentum);
        net.setLearningRate(learningRate, momentum);
    }

    private String getComparison(double error, double prevError){
        String out = String.format("%.4f", error) + "";
        if(prevError != 0){
            out += " (";
            if(prevError > error)
                out += String.format("%.2f", (prevError - error)/prevError*100) + "% better";
            else
                out += String.format("%.2f", (error - prevError)/prevError*100) + "% worse";
            out += ")";
        }
        return out;
    }

    private void updateStatus(final int pass, final int allPasses, final int batch, final int allBatches, final int allRecords){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String text = "pass: " + (pass + 1) + "/" + allPasses;
                if(allBatches != 0)
                    text += " batch: " + (batch + 1) + "/" + allBatches;
                if(allRecords != 0)
                    text += " records: " + allRecords;
                TextView textView = (TextView)findViewById(R.id.statusTextView);
                textView.setText(text );
            }
        });
    }

    private void setStatus(final String message){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView textView = (TextView)findViewById(R.id.statusTextView);
                textView.setText(message);
            }
        });
    }

    private void clearStatus(){
        setStatus("");
    }

    private void log(final String message){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.i("log", message);
                Time now = new Time();
                now.setToNow();
                logList.add(0, now.format("%T") + " " + message);
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void setStatusAndLog(final String message){
        log(message);
        setStatus(message);
    }

    public void abort(View view){
        isNotAborted = false;
        setStatusAndLog("Aborting");
    }

//    private short[] applyShift(short[] data)
//    {
//        short[] out = new short[TABLE_SIZE];
//        for(int i=0; i < TABLE_SIZE; i++)
//            out[i] = (short) (data[i] - shift[i]);
//        return out;
//    }
//
//    private double[] recognise(short[] data) {
//        return net.recognise(
////                applyShift(data)
//                data
//        );
//    }

    private double calculateError(double[] result, int feedback) {
        double error = 0;
        for(double out : result)
            error += out*out;
        error += 1 - 2 * result[feedback];
        return sqrt(error);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_batch_voice_recognition);
        listView = (ListView) findViewById(R.id.logListView);
        adapter = new ArrayAdapter<>(this, R.layout.log_list_item, logList);
        listView.setAdapter(adapter);
    }

    private void calculateShift()
    {
        shift = new short[TABLE_SIZE];
        int[] temp = new int[TABLE_SIZE];
        ArrayList<BatchInfo> batches = BatchesLoader.getBatchList(this);
        int recordsNumber = 0;
        for (BatchInfo batchInfo : batches) {
            if(batchInfo.isInLearningSet) {
                Batch batch = batchInfo.getBatch();
                for (VoiceRecognitionData data : batch.audioRecords) {
                    for (int i = 0; i < TABLE_SIZE; i++)
                        temp[i] += data.getAudioData()[i];
                    recordsNumber++;
                }
            }
        }
        if(recordsNumber != 0)
            for(int i = 0; i < TABLE_SIZE; i++)
                shift[i] = (short)(temp[i]/recordsNumber);
    }

    private void saveShift(){
        try {
            FileOutputStream fileStream = new FileOutputStream(new File(getExternalFilesDir(null), shiftFileName));
            ObjectOutputStream objectStream = new ObjectOutputStream(fileStream);
            objectStream.writeObject(shift);
            objectStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadShift()
    {
        try {
            FileInputStream fileStream = new FileInputStream(new File(getExternalFilesDir(null), shiftFileName));
            ObjectInputStream objectStream = new ObjectInputStream(fileStream);
            shift = (short[])objectStream.readObject();
            objectStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadNetFromStream(InputStream input){
        try {
            byte lengthTable[] = new byte[4];
            input.read(lengthTable);
            ByteBuffer buffer = ByteBuffer.wrap(lengthTable);
            buffer.order(ByteOrder.LITTLE_ENDIAN);
            int length = buffer.getInt();
            byte data[] = new byte[length];
            input.read(data);
            net = new VoiceRecognitionNeuralNet(data);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void saveNetToFile() {
        byte[] save = net.save();
        try {
            FileOutputStream out = new FileOutputStream(new File(getExternalFilesDir(null), netFileName));
            byte lengthTable[] = new byte[4];
            ByteBuffer buffer = ByteBuffer.wrap(lengthTable);
            buffer.order(ByteOrder.LITTLE_ENDIAN);
            buffer.putInt(save.length);
            out.write(lengthTable);
            out.write(save);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy()
    {
        if(net != null)net.destroy();
        super.onDestroy();
    }
}
