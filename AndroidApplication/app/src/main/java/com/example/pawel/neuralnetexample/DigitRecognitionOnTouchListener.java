package com.example.pawel.neuralnetexample;

import androidx.core.view.MotionEventCompat;
import android.view.MotionEvent;
import android.view.View;

import static android.view.View.*;

/**
 * Created by Pawel on 2015-01-12.
 */
public class DigitRecognitionOnTouchListener implements OnTouchListener {
    private float lastX;
    private float lastY;
    private DigitRecognitionNeuralNet net;

    DigitRecognitionOnTouchListener(DigitRecognitionNeuralNet net)
    {
        this.net = net;
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        PaintDigitView paintView = (PaintDigitView) view;
        final int action = MotionEventCompat.getActionMasked(event);
        final float x = event.getX();
        final float y = event.getY();

        switch (action) {
            case MotionEvent.ACTION_DOWN:
                paintView.setPixel(x, y);
                this.lastX = x;
                this.lastY = y;
                break;

            case MotionEvent.ACTION_MOVE:
                paintView.setLine(x, y, lastX, lastY);
                this.lastX = x;
                this.lastY = y;
                break;

            case MotionEvent.ACTION_UP:
                paintView.invalidate();
                double[] result = net.recognise(paintView.getPixels());
                paintView.setOutput(result);
                break;
        }
        return true;
    }
}
