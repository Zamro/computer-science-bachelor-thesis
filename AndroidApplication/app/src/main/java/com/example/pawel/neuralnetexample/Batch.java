package com.example.pawel.neuralnetexample;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by Pawel on 2015-02-08.
 */
public class Batch{
    public ArrayList<VoiceRecognitionData> audioRecords;

    Batch(File file){
        if(file.exists()) {
            load(file);
        }
    }

    Batch(ArrayList<VoiceRecognitionData> audioRecords){
        this.audioRecords = audioRecords;
    }

    @SuppressWarnings("unchecked")
    private void load(File file)
    {
        try {
            FileInputStream fileStream = new FileInputStream(file);
            ObjectInputStream objectStream = new ObjectInputStream(fileStream);
            audioRecords = (ArrayList<VoiceRecognitionData>)objectStream.readObject();
            objectStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void save(File file){
        try {
            FileOutputStream fileStream = new FileOutputStream(file);
            ObjectOutputStream objectStream = new ObjectOutputStream(fileStream);
            objectStream.writeObject(audioRecords);
            objectStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void play(){
        new Thread(){
            @Override
            public void run(){
                VoiceRecordingManager voiceRecordingManager = new VoiceRecordingManager();
                for (VoiceRecognitionData data : audioRecords) {
                    voiceRecordingManager.play(data.getAudioData());
                }
                voiceRecordingManager.release();
            }
        }.start();
    }
}
