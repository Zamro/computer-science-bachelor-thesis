package com.example.pawel.neuralnetexample;

import java.io.File;
import java.io.Serializable;

/**
 * Created by Pawel on 2015-02-08.
 */
public class BatchInfo implements Serializable{
    public String name;
    public boolean isInLearningSet;
    public boolean isInValidationSet;
    public int numberOfRecordings;
    public File file;

    BatchInfo(File file){
        this.file = file;
        name = file.getName();
    }

    public void delete(){
        file.delete();
    }

    public Batch getBatch(){
        return new Batch(file);
    }
}
