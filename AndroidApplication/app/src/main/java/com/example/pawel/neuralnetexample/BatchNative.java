package com.example.pawel.neuralnetexample;

import java.util.ArrayList;

public class BatchNative {
    public short[] audioDataJoined;
    public short[] feedbackJoined;
    public int batchSize;
    BatchNative(Batch batch)
    {
        this(batch.audioRecords);
    }

    BatchNative(ArrayList<VoiceRecognitionData> audioRecords)
    {
        batchSize = audioRecords.size();
        int dataLength = batchSize > 0 ? audioRecords.get(0).getAudioData().length : 0;
        audioDataJoined = new short[batchSize * dataLength];
        feedbackJoined = new short[batchSize];
        for(int i = 0; i < batchSize; i++)
        {
            VoiceRecognitionData recording = audioRecords.get(i);
            short[] audioData = recording.getAudioData();
            System.arraycopy(audioData, 0, audioDataJoined, i * dataLength, dataLength);
            feedbackJoined[i] = (short)recording.getProperFeedback();
        }
    }
}
