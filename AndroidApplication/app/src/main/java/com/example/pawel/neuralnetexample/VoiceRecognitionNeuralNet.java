package com.example.pawel.neuralnetexample;

import java.util.ArrayList;

/**
 * Created by Pawel on 2015-01-09.
 */
public class VoiceRecognitionNeuralNet {
    private long address = 0;

    public VoiceRecognitionNeuralNet()
    {
        address = getVoiceRecognitionNeuralNet();
    }
    private native long getVoiceRecognitionNeuralNet();

    public VoiceRecognitionNeuralNet(byte[] input)
    {
        address = createAndLoadNative(input);
    }
    private native long createAndLoadNative(byte[] input);

    public void destroy()
    {
        if(address != 0)
            destroyNative(address);
        address = 0;
    }
    private native void destroyNative(long address);

    public byte[] save()
    {
        return saveNative(address);
    }
    private native byte[] saveNative(long address);

    public void setLearningRate(double rate, double momentum){setLearningRateNative(address, rate, momentum);}
    private native void setLearningRateNative(long address, double rate, double momentum);

    public void load(byte[] input)
    {
        if(address != 0) loadNative(address, input);
        else address = createAndLoadNative(input);
    }
    private native void loadNative(long address, byte[] input);

    public double[] recognise(short input[])
    {
        return recogniseNative(address, input);
    }
    private native double[] recogniseNative(long address, short input[]);

    public double train(short input[], int feedback)
    {
        return trainNative(address, input, feedback);
    }
    private native double trainNative(long address, short input[], int feedback);

    public double trainBatch(BatchNative data)
    {
        return trainBatchNative(address, data.batchSize, data.audioDataJoined, data.feedbackJoined);
    }
    private native double trainBatchNative(long address, long batchSize, short[] audioData, short[] feedbackData);

    public double batchValidationError(BatchNative data)
    {
        return batchValidationErrorNative(address, data.batchSize, data.audioDataJoined, data.feedbackJoined);
    }
    private native double batchValidationErrorNative(long address, long batchSize, short[] audioData, short[] feedbackData);

}
