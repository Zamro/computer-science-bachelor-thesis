package com.example.pawel.neuralnetexample;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

import static java.lang.Math.sqrt;
import static java.lang.Thread.sleep;


public class BatchesActivity extends Activity {
    ArrayList<BatchInfo> batches;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_batches);
        batches = BatchesLoader.getBatchList(this);
        ListView listView = (ListView) findViewById(R.id.BatchlistView);
        final BatchInfoAdapter adapter = new BatchInfoAdapter(this, android.R.layout.simple_list_item_1, batches);
        listView.setAdapter(adapter);
    }

    @Override
    public void onDestroy()
    {
        BatchesLoader.saveBatchList(this, batches);
        super.onDestroy();
    }

    public void addBatch(View view){
        Intent intent = new Intent(this, NewBatchActivity.class);
        startActivity(intent);
    }
}
