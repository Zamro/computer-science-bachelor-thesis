package com.example.pawel.neuralnetexample;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

import static com.example.pawel.neuralnetexample.VoiceRecordingManager.TABLE_SIZE;
import static java.lang.Math.sqrt;
import static java.lang.Thread.sleep;


public class VoiceRecognitionActivity extends Activity {
//    private final String fileName = "singleLayerVoiceNeuralNet.save";
    private final String netFileName = "dualLayerVoiceNeuralNet.save";
//    private final String fileName = "voiceNeuralNet.save";
    private final String audioFileName = "audio.save";
    private final String shiftFileName = "shift.save";
    private boolean isRecordValid = false;
    ArrayList<VoiceRecognitionData> voiceRecognitionData = new ArrayList<VoiceRecognitionData>();
    VoiceRecognitionNeuralNet net;
    short[] shift;
    VoiceRecordingManager voiceRecordingManager = new VoiceRecordingManager();

    static {
        System.loadLibrary("NeuralNet");
    }

    private short[] audioData = new short[TABLE_SIZE];

    private void setButtonsEnabled(final boolean enabled)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.recordButton).setEnabled(enabled);
                findViewById(R.id.playButton).setEnabled(enabled && isRecordValid);
                LinearLayout layout = (LinearLayout) findViewById(R.id.voiceButtonsLayout);
                for (int i = 0; i < layout.getChildCount(); i++) {
                    layout.getChildAt(i).setEnabled(enabled && isRecordValid);
                }
            }
        });
    }

    public void record(View view)
    {
        new Thread(){
            @Override
            public void run(){
                setButtonsEnabled(false);
                audioData = voiceRecordingManager.record();
                setOutput(recognise(audioData));
                setButtonsEnabled(true);
            }
        }.start();
    }

    private short[] applyShift(short[] data)
    {
        short[] out = new short[TABLE_SIZE];
        for(int i=0; i < TABLE_SIZE; i++)
            out[i] = (short) (data[i] - shift[i]);
        return out;
    }

    private double[] recognise(short[] data) {
        isRecordValid = true;
        //TODO: investigate if shifting has sense, if yes, then apply it everywhere, not only here.
//        double[] result = net.recognise(applyShift(data));
        double[] result = net.recognise(data);
        for(double out:result)
            if(Double.isNaN(out)) isRecordValid = false;
        return result;
    }

    public void setOutput(final double[] output)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayout layout = (LinearLayout) findViewById(R.id.voiceButtonsLayout);
                for (int i = 0; i < layout.getChildCount(); i++) {
                    Log.i("Output: ", ""+i+" "+output[i]);
                    layout.getChildAt(i).getBackground().setColorFilter(Color.argb((int) (output[i] * 255), 0, 255, 0), PorterDuff.Mode.SRC);
                }
                layout.invalidate();
            }
        });
    }

    public void clearData() {
        setOutput(new double[5]);
        isRecordValid = false;
        setButtonsEnabled(true);
    }

    public void play(View view)
    {
        new Thread(){
            @Override
            public void run() {
                setButtonsEnabled(false);
                voiceRecordingManager.play(audioData);
                setButtonsEnabled(true);
            }
        }.start();
    }

    public void playAll(View view)
    {
        new Thread(){
            @Override
            public void run(){
                setButtonsEnabled(false);
                for (VoiceRecognitionData data : voiceRecognitionData) {
                    voiceRecordingManager.play(data.getAudioData());
                }
                setButtonsEnabled(true);
            }
        }.start();
    }

    public void giveFeedback(View button)
    {
        int feedback = Integer.parseInt((String) button.getTag());
        net.train(audioData, feedback);
        voiceRecognitionData.add(new VoiceRecognitionData(audioData, feedback));
        clearData();
    }

    public void learnAll(View button)
    {
        BatchNative batch = new BatchNative(voiceRecognitionData);
        for(int j=0;j<1;j++) {
            Log.i("Learn all: ", "Starting pass number:" + j);
            double batchError = net.trainBatch(batch);
            Log.i("batch error", "" + batchError);
        }
        Log.i("Learn all: ", "Learning finished");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_recognition);
        setButtonsEnabled(true);
        loadAudio();
        loadNeuralNet();
    }

    private void loadAudio(){
        File file = new File(getExternalFilesDir(null), audioFileName);
        if(file.exists()) {
            Log.i("Loading audio:", "file existing");
            loadAudioFromFile(file);
        }else{
            Log.i("Loading audio:", "file not existing");
        }
    }

    private void loadNeuralNet() {
        File file = new File(getExternalFilesDir(null), netFileName);
        if(!file.exists())
        {
            Log.i("Loading net:", "creating new net");
            net = new VoiceRecognitionNeuralNet();
            calculateAndSaveShift();
        }
        else
        {
            Log.i("Loading net:", "loading existing net");
            try {
                FileInputStream input = new FileInputStream(file);
                loadNetFromStream(input);
                loadShift();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        double learningRate = 0.1;
        double momentum = 0.9;
        Log.i("VoiceRecognition", "   Learning rate: " + learningRate + ", momentum: " + momentum);
        net.setLearningRate(learningRate, momentum);
    }

    private void calculateAndSaveShift()
    {
        shift = new short[TABLE_SIZE];
        if(! voiceRecognitionData.isEmpty())
        {
            int[] temp = new int[TABLE_SIZE];
            for (VoiceRecognitionData data : voiceRecognitionData){
                for(int i = 0; i < TABLE_SIZE; i++)
                    temp[i] += data.getAudioData()[i];
            }
            for(int i = 0; i < TABLE_SIZE; i++)
                shift[i] = (short)(temp[i]/voiceRecognitionData.size());
        }

        try {
            FileOutputStream fileStream = new FileOutputStream(new File(getFilesDir(), shiftFileName));
            ObjectOutputStream objectStream = new ObjectOutputStream(fileStream);
            objectStream.writeObject(shift);
            objectStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadShift()
    {
        try {
            FileInputStream fileStream = new FileInputStream(new File(getFilesDir(), shiftFileName));
            ObjectInputStream objectStream = new ObjectInputStream(fileStream);
            shift = (short[])objectStream.readObject();
            objectStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadNetFromStream(InputStream input){
        try {
            byte lengthTable[] = new byte[4];
            input.read(lengthTable);
            ByteBuffer buffer = ByteBuffer.wrap(lengthTable);
            buffer.order(ByteOrder.LITTLE_ENDIAN);
            int length = buffer.getInt();
            byte[] data = new byte[length];
            input.read(data);
            net = new VoiceRecognitionNeuralNet(data);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_voice_recognition, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return item.getItemId() == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy()
    {
        saveNetToFile(net, new File(getExternalFilesDir(null), netFileName));
        saveAudioToFile(new File(getExternalFilesDir(null), audioFileName));
        net.destroy();
        voiceRecordingManager.release();
        super.onDestroy();
    }

    private void saveAudioToFile(File file)
    {
        try {
            FileOutputStream fileStream = new FileOutputStream(file);
            ObjectOutputStream objectStream = new ObjectOutputStream(fileStream);
            objectStream.writeObject(voiceRecognitionData);
            objectStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    private void loadAudioFromFile(File file)
    {
        try {
            FileInputStream fileStream = new FileInputStream(file);
            ObjectInputStream objectStream = new ObjectInputStream(fileStream);
            voiceRecognitionData = (ArrayList<VoiceRecognitionData>)objectStream.readObject();
            objectStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveNetToFile(VoiceRecognitionNeuralNet net, File file) {
        byte[] save = net.save();
        try {
            FileOutputStream out = new FileOutputStream(file);
            byte lengthTable[] = new byte[4];
            ByteBuffer buffer = ByteBuffer.wrap(lengthTable);
            buffer.order(ByteOrder.LITTLE_ENDIAN);
            buffer.putInt(save.length);
            out.write(lengthTable);
            out.write(save);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
