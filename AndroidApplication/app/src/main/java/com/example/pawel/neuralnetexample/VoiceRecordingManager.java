package com.example.pawel.neuralnetexample;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaRecorder;

import static java.lang.Thread.sleep;

/**
 * Created by Pawel on 2015-02-08.
 */
public class VoiceRecordingManager {
    private static final int RECORDER_SAMPLE_RATE = 44100;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    private static final int BYTES_PER_SAMPLE = 2;
    private static final int SAMPLES_PER_FFT = 2048;
    private static final int NUMBER_OF_FFTS = 40;
    public static final int TABLE_SIZE = NUMBER_OF_FFTS*SAMPLES_PER_FFT;
    public static final float RECORD_LENGTH_IN_SECONDS = NUMBER_OF_FFTS*SAMPLES_PER_FFT/RECORDER_SAMPLE_RATE;
    private static final int BYTE_SIZE = TABLE_SIZE * BYTES_PER_SAMPLE;

    private AudioRecord recorder = new AudioRecord(MediaRecorder.AudioSource.DEFAULT,
            RECORDER_SAMPLE_RATE, RECORDER_CHANNELS,
            RECORDER_AUDIO_ENCODING,
            BYTE_SIZE);
    private AudioTrack track = new AudioTrack(android.media.AudioManager.STREAM_MUSIC, RECORDER_SAMPLE_RATE,
            AudioFormat.CHANNEL_OUT_MONO, RECORDER_AUDIO_ENCODING, BYTE_SIZE, AudioTrack.MODE_STREAM);

    public short[] record() {
        short[] audioData = new short[TABLE_SIZE];
        recorder.startRecording();
        recorder.read(audioData, 0, TABLE_SIZE);
        recorder.stop();
        return audioData;
    }

    public void play(short[] audioData)
    {
        track.play();
        track.write(audioData, 0, TABLE_SIZE);
        try {
            sleep((int) (RECORD_LENGTH_IN_SECONDS * 1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        track.stop();
    }

    public void release()
    {
        recorder.release();
        track.release();
    }
}
