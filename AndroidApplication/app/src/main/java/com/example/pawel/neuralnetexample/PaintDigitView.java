package com.example.pawel.neuralnetexample;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * Created by Pawel on 2015-01-11.
 */
public class PaintDigitView extends View {
    private int RENDER_MARGIN = 20;
    private Bitmap bitmap;
    private Bitmap scaledBitmap;
    private Canvas canvas;
    private Paint paint = new Paint();
    private float scale;
    private float xDif;
    private float yDif;

    public PaintDigitView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.PaintDigitView,
                0, 0);

        bitmap = Bitmap.createBitmap(
                a.getInteger(R.styleable.PaintDigitView_bitmapWidth, 32),
                a.getInteger(R.styleable.PaintDigitView_bitmapHeight, 48),
                Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bitmap);
        a.recycle();
        clear();
        setBackgroundColor(Color.WHITE);
    }

    public void clear()
    {
        bitmap.eraseColor(Color.rgb(245, 255, 245));
        invalidate();
    }

    public void setNeuralNet(DigitRecognitionNeuralNet net)
    {
        setOnTouchListener(new DigitRecognitionOnTouchListener(net));
    }

    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        if (scaledBitmap == null) {
            scale = min(getWidth() / (float) bitmap.getWidth(), getHeight() / (float) bitmap.getHeight());
            xDif = (getWidth() - scale * bitmap.getWidth()) / 2;
            yDif = (getHeight() - scale * bitmap.getHeight()) / 2;
            RENDER_MARGIN = (int) (scale) + 1;
            paint.setStrokeWidth(bitmap.getWidth()/16f);
        }
        scaledBitmap = Bitmap.createScaledBitmap(bitmap, (int) (scale * bitmap.getWidth()), (int) (scale * bitmap.getHeight()), false);
        canvas.drawBitmap(scaledBitmap, xDif, yDif, paint);
    }

    public void setOutput(double[] output) {
        ((DigitRecognitionActivity)getContext()).setOutput(output);
    }

    public void setPixel(float x, float y)
    {
        if(x<scale*bitmap.getWidth()&&y<scale*bitmap.getHeight())
        {
            bitmap.setPixel((int)((x - xDif)/scale), (int)((y-yDif)/scale), Color.BLACK);
            invalidate(new Rect((int) x - RENDER_MARGIN,
                    (int) y - RENDER_MARGIN,
                    (int) x + RENDER_MARGIN,
                    (int) y + RENDER_MARGIN));
        }
    }

    public void setLine(float x1, float y1, float x2, float y2)
    {
        canvas.drawLine((x1 - xDif)/scale,
                (y1 - yDif)/scale,
                (x2 - xDif)/scale,
                (y2 - yDif)/scale,
                paint);
        invalidate(new Rect((int) min(x1, x2) - RENDER_MARGIN,
                (int) min(y1, y2) - RENDER_MARGIN,
                (int) max(x1, x2) + RENDER_MARGIN,
                (int) max(y1, y2) + RENDER_MARGIN));
    }

    public double[] getPixels()
    {
        int rawPixels[] = new int[bitmap.getWidth() * bitmap.getHeight()];
        bitmap.getPixels(rawPixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        double convertedPixels[] = new double[bitmap.getWidth() * bitmap.getHeight()];
        for(int i=0;i<bitmap.getWidth() * bitmap.getHeight();i++)
            convertedPixels[i] = (rawPixels[i] == Color.BLACK ? 1.0f : 0.0f);
        return convertedPixels;
    }
}
