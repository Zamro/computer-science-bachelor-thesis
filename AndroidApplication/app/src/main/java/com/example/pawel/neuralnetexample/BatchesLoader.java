package com.example.pawel.neuralnetexample;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by Pawel on 2015-02-08.
 */
public class BatchesLoader {
    private static final String batchesFileName = "batches.save";

    @SuppressWarnings("unchecked")
    static ArrayList<BatchInfo> getBatchList(Context context){
        File batchesDir;
        batchesDir = new File(context.getExternalFilesDir(null), "batches");
        if (!batchesDir.exists())
            batchesDir.mkdir();
        ArrayList<BatchInfo> batches = new ArrayList<>();
        File batchesFile = new File(context.getExternalFilesDir(null), batchesFileName);
        if(batchesFile.exists()) {
            try {
                FileInputStream fileStream = new FileInputStream(batchesFile);
                ObjectInputStream objectStream = new ObjectInputStream(fileStream);
                batches = (ArrayList<BatchInfo>)objectStream.readObject();
                objectStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            batches = new ArrayList<>();
        }
        for(File file : batchesDir.listFiles())
        {
            boolean isContained = false;
            for(BatchInfo info : batches)
                if(info.name.equals(file.getName()))
                {
                    isContained = true;
                    break;
                }
            if(!isContained)
                batches.add(new BatchInfo(file));
        }
        ArrayList<BatchInfo> toRemove = new ArrayList<BatchInfo>();
        for(BatchInfo info: batches)
        {
            File file = new File(batchesDir, info.name);
            if(!file.exists())
                toRemove.add(info);
        }
        batches.removeAll(toRemove);
        for(BatchInfo info: batches)
        {
            if(info.numberOfRecordings == 0)
                info.numberOfRecordings = info.getBatch().audioRecords.size();
        }
        return batches;
    }

    static void saveBatchList(Context context, ArrayList<BatchInfo> batches){
        File batchesFile = new File(context.getExternalFilesDir(null), batchesFileName);
        try {
            FileOutputStream fileStream = new FileOutputStream(batchesFile);
            ObjectOutputStream objectStream = new ObjectOutputStream(fileStream);
            objectStream.writeObject(batches);
            objectStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
