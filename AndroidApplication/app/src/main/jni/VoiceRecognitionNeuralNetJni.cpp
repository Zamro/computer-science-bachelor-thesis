#include "VoiceRecognitionNeuralNetJni.hpp"
#include "NeuralNetLib/NeuralNet.hpp"
#include "NeuralNetLib/Neurons/SoftmaxNeuron.hpp"
#include "NeuralNetLib/Neurons/SigmoidalNeuron.hpp"
#include <NeuralNetLib/LearningAlgorithms/Backprop.hpp>
#include "fft.hpp"
#include <android/log.h>
#include <cmath>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <climits>

unsigned const SAMPLES_PER_FFT = 2048;
unsigned const LOG_OF_SAMPLES_PER_FFT = 11;
unsigned const NUMBER_OF_FFTS = 40;
unsigned const RAW_DATA_SIZE = SAMPLES_PER_FFT * NUMBER_OF_FFTS;
unsigned const LOWEST_FREQUENCY = 14;     //301,5 Hz
unsigned const HIGHEST_FREQUENCY = 158;   //3402 Hz
unsigned const NUMBER_OF_FREQUENCIES = (HIGHEST_FREQUENCY - LOWEST_FREQUENCY + 1);
unsigned const NUMBER_OF_INPUTS = NUMBER_OF_FFTS * NUMBER_OF_FREQUENCIES;
unsigned const NUMBER_OF_OUTPUTS = 5;

unsigned const DEFAULT_LEARNING_RATE = 1;
const ArithmeticType DEFAULT_MOMENTUM = 0.8;

//typedef NeuralNet<SigmoidalNeuron, SoftmaxNeuron, NUMBER_OF_INPUTS, 256, 256, NUMBER_OF_OUTPUTS> VoiceRecognitionNet;
typedef NeuralNet<SigmoidalNeuron, SoftmaxNeuron, NUMBER_OF_FREQUENCIES, 64, NUMBER_OF_OUTPUTS> VoiceRecognitionNet;
//typedef NeuralNet<SigmoidalNeuron, SoftmaxNeuron, NUMBER_OF_INPUTS, NUMBER_OF_OUTPUTS> VoiceRecognitionNet;

std::array<ArithmeticType, NUMBER_OF_OUTPUTS> getProperOutputFor(unsigned index)
{
    std::array<ArithmeticType, NUMBER_OF_OUTPUTS> ret{{}};
    ret[index] = 1;
    return ret;
}

std::string convertByteArrayToString(JNIEnv* env, jbyteArray input)
{
    unsigned length = env->GetArrayLength(input);
    jbyte* adr = env->GetByteArrayElements(input, 0);
    return std::string((char*)adr, length);
}

JNIEXPORT jlong JNICALL Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_getVoiceRecognitionNeuralNet
    (JNIEnv* env, jobject javaThis)
{
    auto net = new VoiceRecognitionNet();
    net->setLearningAlgorithm(Backprop {{DEFAULT_LEARNING_RATE, DEFAULT_MOMENTUM}});
    return uintptr_t(net);
}

JNIEXPORT jlong JNICALL
    Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_createAndLoadNative
    (JNIEnv* env, jobject javaThis, jbyteArray input)
{
    auto net = new VoiceRecognitionNet(convertByteArrayToString(env, input));
    net->setLearningAlgorithm(Backprop {{DEFAULT_LEARNING_RATE, DEFAULT_MOMENTUM}});
    return uintptr_t(net);
}

JNIEXPORT void JNICALL
    Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_destroyNative
    (JNIEnv* env, jobject javaThis, jlong address)
{
    auto net = (VoiceRecognitionNet*) uintptr_t(address);
    delete net;
}

std::array<ArithmeticType, NUMBER_OF_INPUTS> processRawInput(const std::array<short, RAW_DATA_SIZE>& rawData)
{
    std::array<ArithmeticType, NUMBER_OF_INPUTS> out{};
    for(int fftNumber = 0; fftNumber<NUMBER_OF_FFTS; fftNumber++)
    {
        std::complex<ArithmeticType> data[SAMPLES_PER_FFT];
        for(unsigned int i=0; i < SAMPLES_PER_FFT; i++)
            data[i] = rawData[fftNumber * NUMBER_OF_FREQUENCIES + i];
        FFT(LOG_OF_SAMPLES_PER_FFT, data);
        for(unsigned int i=0; i < NUMBER_OF_FREQUENCIES; i++)
        {
            out[fftNumber * NUMBER_OF_FREQUENCIES + i] = 1000 * abs(data[LOWEST_FREQUENCY + i])/SHRT_MAX;
            if(!std::isnormal(out[fftNumber * NUMBER_OF_FREQUENCIES + i]))
            {
                std::stringstream ss;
                ss<<(int)(44100/2048*(LOWEST_FREQUENCY + i))<<"Hz; "<<fftNumber * NUMBER_OF_FREQUENCIES + i<<" ; "<<out[fftNumber * NUMBER_OF_FREQUENCIES + i];
                __android_log_write(ANDROID_LOG_ERROR, "Amplitude after fast fourier transform is invalid: ", ss.str().data());
            }
        }
    }
    return out;
}

std::array<ArithmeticType, NUMBER_OF_FREQUENCIES> getMeans(const std::array<ArithmeticType, NUMBER_OF_INPUTS>& data)
{
    std::array<ArithmeticType, NUMBER_OF_FREQUENCIES> out{{}};
    for(int i=0;i<NUMBER_OF_FREQUENCIES;i++)
    {
        for(int j=0;j<NUMBER_OF_FFTS;j++)
            out[i] = data[i + j* NUMBER_OF_FREQUENCIES];
        out[i] /= NUMBER_OF_FFTS;
    }
    return out;
}

std::array<ArithmeticType, NUMBER_OF_FREQUENCIES> getMaxes(const std::array<ArithmeticType, NUMBER_OF_INPUTS>& data)
{
    std::array<ArithmeticType, NUMBER_OF_FREQUENCIES> out{{}};
    for(int i=0;i<NUMBER_OF_FREQUENCIES;i++)
    {
        for(int j=0;j<NUMBER_OF_FFTS;j++)
            if(out[i] < data[i + j* NUMBER_OF_FREQUENCIES])
                out[i] = data[i + j* NUMBER_OF_FREQUENCIES];
    }
    return out;
}

std::array<ArithmeticType, NUMBER_OF_FREQUENCIES> getData(JNIEnv* env, jshortArray input, int dataChunkNumber = 0)
{
    std::array<short, RAW_DATA_SIZE> rawData{};
    env->GetShortArrayRegion( input, dataChunkNumber * RAW_DATA_SIZE, RAW_DATA_SIZE, &rawData[0] );
    auto preprocessedData = processRawInput(rawData);
    return getMaxes(preprocessedData);
}

VoiceRecognitionNet::Data getBatchData(JNIEnv* env, jlong batchSize, jshortArray audioData, jshortArray feedbackData)
{
    VoiceRecognitionNet::Data data(batchSize);
    for(int i = 0; i < batchSize; i++)
    {
        data[i].first = getData(env, audioData, i);

        short feedback;
        env->GetShortArrayRegion(feedbackData, i, 1, &feedback );
        data[i].second = getProperOutputFor(feedback);
    }
    return data;
}

JNIEXPORT jdoubleArray JNICALL
    Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_recogniseNative
    (JNIEnv* env, jobject javaThis, jlong address, jshortArray input)
{
    auto net = (VoiceRecognitionNet*) uintptr_t(address);
    auto data = getData(env, input);
    auto output = net->predict(data);

    jdoubleArray jniOutput = env->NewDoubleArray(NUMBER_OF_OUTPUTS);
    env->SetDoubleArrayRegion(jniOutput, 0, NUMBER_OF_OUTPUTS, output.begin());
    return jniOutput;
}

JNIEXPORT double JNICALL
Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_trainNative
        (JNIEnv* env, jobject javaThis, jlong address, jshortArray input, jint feedback)
{
    auto net = (VoiceRecognitionNet*) uintptr_t(address);
    auto data = getData(env, input);
    return net->train(data, getProperOutputFor(feedback));
}

JNIEXPORT double JNICALL
    Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_trainBatchNative
        (JNIEnv* env, jobject javaThis, jlong address, jlong batchSize, jshortArray audioData, jshortArray feedbackData)
{
    auto net = (VoiceRecognitionNet*) uintptr_t(address);
    VoiceRecognitionNet::Data data = getBatchData(env, batchSize, audioData, feedbackData);
    return net->train(data);
}

JNIEXPORT double JNICALL
    Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_batchValidationErrorNative
        (JNIEnv* env, jobject javaThis, jlong address, jlong batchSize, jshortArray audioData, jshortArray feedbackData)
{
    auto net = (VoiceRecognitionNet*) uintptr_t(address);
    VoiceRecognitionNet::Data data = getBatchData(env, batchSize, audioData, feedbackData);

    auto errorFunction = StandardError<typename VoiceRecognitionNet::OutputType>{};
    errorFunction.setNeuralNet(net);
    //TODO: it shouldn't be needed, overwriting vs virtual interface should be fixed.
    ErrorFunction<typename VoiceRecognitionNet::OutputType>& errorFunctionRef = errorFunction;
    return errorFunctionRef.error(*net, data);
}

JNIEXPORT void JNICALL Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_setLearningRateNative
    (JNIEnv* env, jobject javaThis, jlong address, jdouble rate, jdouble momentum)
{
    auto net = (VoiceRecognitionNet*) uintptr_t(address);
    net->setLearningAlgorithm(Backprop{{rate, momentum}});
}

JNIEXPORT void JNICALL
    Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_loadNative
    (JNIEnv* env, jobject javaThis, jlong address, jbyteArray input)
{
    auto net = (VoiceRecognitionNet*) uintptr_t(address);
    net->load(convertByteArrayToString(env, input));
}

JNIEXPORT jbyteArray JNICALL
    Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_saveNative
    (JNIEnv* env, jobject javaThis, jlong address)
{
    auto net = (VoiceRecognitionNet*) uintptr_t(address);
    auto save = net->saveToString();
    jbyteArray out = env->NewByteArray(save.length());
    env->SetByteArrayRegion(out, 0, save.length(), (jbyte*)save.c_str());
    return out;
}