#include <jni.h>
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jlong JNICALL
    Java_com_example_pawel_neuralnetexample_DigitRecognitionNeuralNet_getDigitRecognitionNeuralNet
    (JNIEnv* env, jobject javaThis);

JNIEXPORT void JNICALL
    Java_com_example_pawel_neuralnetexample_DigitRecognitionNeuralNet_destroyNative
    (JNIEnv* env, jobject javaThis, jlong address);

JNIEXPORT void JNICALL
    Java_com_example_pawel_neuralnetexample_DigitRecognitionNeuralNet_loadNative
    (JNIEnv* env, jobject javaThis, jlong address, jbyteArray input);

JNIEXPORT jbyteArray JNICALL
    Java_com_example_pawel_neuralnetexample_DigitRecognitionNeuralNet_saveNative
    (JNIEnv* env, jobject javaThis, jlong address);

JNIEXPORT jdoubleArray JNICALL
    Java_com_example_pawel_neuralnetexample_DigitRecognitionNeuralNet_recogniseNative
    (JNIEnv* env, jobject javaThis, jlong address, jdoubleArray input);

JNIEXPORT double JNICALL
    Java_com_example_pawel_neuralnetexample_DigitRecognitionNeuralNet_trainNative
    (JNIEnv* env, jobject javaThis, jlong address, jdoubleArray input, jint feedback);

#ifdef __cplusplus
}
#endif
