#include "DigitRecognitionNeuralNetJni.hpp"
#include <NeuralNetLib/NeuralNet.hpp>
#include <NeuralNetLib/Neurons/SigmoidalNeuron.hpp>
#include <NeuralNetLib/Neurons/SoftmaxNeuron.hpp>
#include <NeuralNetLib/LearningAlgorithms/Backprop.hpp>
#include "NeuralNetLib/DigitImageNormaliser.hpp"
#include <string>
#include <vector>
#include <algorithm>

unsigned const width = 32;
unsigned const height = 48;
unsigned const bitmapWidth = 2*width;
unsigned const bitmapHeight = 2*height;
unsigned const inputs = width*height;
unsigned const bitmapInputs = bitmapWidth*bitmapHeight;
unsigned const outputs = 10;

typedef NeuralNet<SigmoidalNeuron, SoftmaxNeuron, inputs, outputs> DigitRecognitionNet;

std::array<ArithmeticType, outputs> getProperOutputFor(char letter)
{
    std::array<ArithmeticType, outputs> ret{{}};
    ret[letter-'0'] = 1;
    return ret;
}

JNIEXPORT jlong JNICALL Java_com_example_pawel_neuralnetexample_DigitRecognitionNeuralNet_getDigitRecognitionNeuralNet
    (JNIEnv* env, jobject javaThis)
{
    auto net = new DigitRecognitionNet();
    net->setLearningAlgorithm(Backprop{{0.05,0.2}});
    return uintptr_t(net);
}

JNIEXPORT void JNICALL
    Java_com_example_pawel_neuralnetexample_DigitRecognitionNeuralNet_destroyNative
    (JNIEnv* env, jobject javaThis, jlong address)
{
    auto net = (DigitRecognitionNet*) uintptr_t(address);
    delete net;
}

std::array<ArithmeticType, inputs> normalizeData(JNIEnv* env, jdoubleArray input)
{
    std::array<ArithmeticType, bitmapInputs> data;
    env->GetDoubleArrayRegion(input, 0, bitmapInputs, &data[0] );
    return DigitImageNormaliser<bitmapWidth, bitmapHeight, width, height>::normalise(data);
}

JNIEXPORT jdoubleArray JNICALL
    Java_com_example_pawel_neuralnetexample_DigitRecognitionNeuralNet_recogniseNative
    (JNIEnv* env, jobject javaThis, jlong address, jdoubleArray input)
{
    auto net = (DigitRecognitionNet*) uintptr_t(address);
    auto output = net->predict(normalizeData(env, input));

    jdoubleArray jniOutput = env->NewDoubleArray(outputs);
    env->SetDoubleArrayRegion(jniOutput, 0, outputs, output.begin());
    return jniOutput;
}

JNIEXPORT double JNICALL
    Java_com_example_pawel_neuralnetexample_DigitRecognitionNeuralNet_trainNative
    (JNIEnv* env, jobject javaThis, jlong address, jdoubleArray input, jint feedback)
{
    auto net = (DigitRecognitionNet*) uintptr_t(address);
    return net->train(normalizeData(env, input), getProperOutputFor(feedback));
}

JNIEXPORT void JNICALL
    Java_com_example_pawel_neuralnetexample_DigitRecognitionNeuralNet_loadNative
    (JNIEnv* env, jobject javaThis, jlong address, jbyteArray input)
{
    auto net = (DigitRecognitionNet*) uintptr_t(address);
    unsigned length = env->GetArrayLength(input);
    jbyte* adr = env->GetByteArrayElements(input, 0);
    std::string data((char*)adr, length);
    net->load(data);
}

JNIEXPORT jbyteArray JNICALL
    Java_com_example_pawel_neuralnetexample_DigitRecognitionNeuralNet_saveNative
    (JNIEnv* env, jobject javaThis, jlong address)
{
    auto net = (DigitRecognitionNet*) uintptr_t(address);
    auto save = net->saveToString();
    jbyteArray out = env->NewByteArray(save.length());
    env->SetByteArrayRegion(out, 0, save.length(), (jbyte*)save.c_str());
    return out;
}