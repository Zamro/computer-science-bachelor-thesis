#include <jni.h>
#pragma once

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT jlong JNICALL
    Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_getVoiceRecognitionNeuralNet
    (JNIEnv* env, jobject javaThis);

JNIEXPORT void JNICALL
    Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_destroyNative
    (JNIEnv* env, jobject javaThis, jlong address);

JNIEXPORT jdoubleArray JNICALL
    Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_recogniseNative
    (JNIEnv* env, jobject javaThis, jlong address, jshortArray input);

JNIEXPORT double JNICALL
Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_trainNative
        (JNIEnv* env, jobject javaThis, jlong address, jshortArray input, jint feedback);

JNIEXPORT double JNICALL
    Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_trainBatchNative
    (JNIEnv* env, jobject javaThis, jlong address, jlong batchSize, jshortArray audioData, jshortArray feedbackData);

JNIEXPORT double JNICALL
    Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_batchValidationErrorNative
    (JNIEnv* env, jobject javaThis, jlong address, jlong batchSize, jshortArray audioData, jshortArray feedbackData);

JNIEXPORT void JNICALL Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_setLearningRateNative
    (JNIEnv* env, jobject javaThis, jlong address, jdouble rate, jdouble momentum);

JNIEXPORT void JNICALL
    Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_loadNative
    (JNIEnv* env, jobject javaThis, jlong address, jbyteArray input);

JNIEXPORT jlong JNICALL
    Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_createAndLoadNative
    (JNIEnv* env, jobject javaThis, jbyteArray input);

JNIEXPORT jbyteArray JNICALL
    Java_com_example_pawel_neuralnetexample_VoiceRecognitionNeuralNet_saveNative
    (JNIEnv* env, jobject javaThis, jlong address);
#ifdef __cplusplus
}
#endif
